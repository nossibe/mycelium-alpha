<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201230103540 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE city (id INT AUTO_INCREMENT NOT NULL, postal_code VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE merchant (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, city_id INT NOT NULL, address VARCHAR(2000) NOT NULL, business_name VARCHAR(255) NOT NULL, published TINYINT(1) NOT NULL, slugify VARCHAR(255) NOT NULL, paypal_email VARCHAR(255) DEFAULT NULL, filename VARCHAR(255) NOT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_74AB25E1A76ED395 (user_id), INDEX IDX_74AB25E18BAC62AF (city_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE merchant_product_category (merchant_id INT NOT NULL, product_category_id INT NOT NULL, INDEX IDX_ACE1A3A96796D554 (merchant_id), INDEX IDX_ACE1A3A9BE6903FD (product_category_id), PRIMARY KEY(merchant_id, product_category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL, object_changed VARCHAR(255) NOT NULL, object_id INT NOT NULL, event_type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, name VARCHAR(255) NOT NULL, ref VARCHAR(255) DEFAULT NULL, published TINYINT(1) DEFAULT NULL, filename VARCHAR(255) DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_D34A04AD12469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_category (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, published TINYINT(1) DEFAULT NULL, INDEX IDX_CDFC7356727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_search (id INT AUTO_INCREMENT NOT NULL, merchant_id INT NOT NULL, product_id INT DEFAULT NULL, quantity_unit_id INT DEFAULT NULL, quantity INT NOT NULL, dead_line DATETIME DEFAULT NULL, transporter_gift VARCHAR(2000) DEFAULT NULL, published TINYINT(1) DEFAULT NULL, clarifications LONGTEXT DEFAULT NULL, closed_date_time DATETIME DEFAULT NULL, INDEX IDX_D68C9A036796D554 (merchant_id), INDEX IDX_D68C9A034584665A (product_id), INDEX IDX_D68C9A03B09B908 (quantity_unit_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quantity_unit (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sell_proposal (id INT AUTO_INCREMENT NOT NULL, seller_id INT NOT NULL, product_search_id INT DEFAULT NULL, quantity_unit_id INT DEFAULT NULL, unit_price VARCHAR(255) NOT NULL, qty_available INT NOT NULL, published TINYINT(1) DEFAULT NULL, INDEX IDX_5E82686D8DE820D9 (seller_id), INDEX IDX_5E82686DBA7D624 (product_search_id), INDEX IDX_5E82686DB09B908 (quantity_unit_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transport_proposal (id INT AUTO_INCREMENT NOT NULL, transporter_id INT DEFAULT NULL, transport_search_id INT NOT NULL, sell_proposal_id INT DEFAULT NULL, recovery_time DATETIME NOT NULL, delivery_time DATETIME NOT NULL, buyer_agreement TINYINT(1) DEFAULT NULL, published TINYINT(1) DEFAULT NULL, in_delivering DATETIME DEFAULT NULL, delivered DATETIME DEFAULT NULL, INDEX IDX_75CB81384F335C8B (transporter_id), INDEX IDX_75CB8138F3C4E632 (transport_search_id), INDEX IDX_75CB813888DC9EBE (sell_proposal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transport_search (id INT AUTO_INCREMENT NOT NULL, sell_proposal_id INT NOT NULL, published TINYINT(1) NOT NULL, date DATETIME NOT NULL, INDEX IDX_70BAE2E88DC9EBE (sell_proposal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transporter (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, residence_city_id INT DEFAULT NULL, work_city_id INT DEFAULT NULL, score INT DEFAULT NULL, published TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_A036E2D4A76ED395 (user_id), INDEX IDX_A036E2D4C52E0EE9 (residence_city_id), INDEX IDX_A036E2D45E67DA8D (work_city_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transporter_city (transporter_id INT NOT NULL, city_id INT NOT NULL, INDEX IDX_FE9C07CE4F335C8B (transporter_id), INDEX IDX_FE9C07CE8BAC62AF (city_id), PRIMARY KEY(transporter_id, city_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, user_name VARCHAR(255) NOT NULL, full_name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, roles JSON DEFAULT NULL, inscription_date DATETIME NOT NULL, published TINYINT(1) NOT NULL, api_token VARCHAR(255) DEFAULT NULL, activation_token VARCHAR(255) DEFAULT NULL, reset_token VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D6497BA2F5EB (api_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_notification (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, notification_id INT NOT NULL, read_at DATETIME DEFAULT NULL, INDEX IDX_3F980AC8A76ED395 (user_id), INDEX IDX_3F980AC8EF1A9D84 (notification_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE merchant ADD CONSTRAINT FK_74AB25E1A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE merchant ADD CONSTRAINT FK_74AB25E18BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE merchant_product_category ADD CONSTRAINT FK_ACE1A3A96796D554 FOREIGN KEY (merchant_id) REFERENCES merchant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE merchant_product_category ADD CONSTRAINT FK_ACE1A3A9BE6903FD FOREIGN KEY (product_category_id) REFERENCES product_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD12469DE2 FOREIGN KEY (category_id) REFERENCES product_category (id)');
        $this->addSql('ALTER TABLE product_category ADD CONSTRAINT FK_CDFC7356727ACA70 FOREIGN KEY (parent_id) REFERENCES product_category (id)');
        $this->addSql('ALTER TABLE product_search ADD CONSTRAINT FK_D68C9A036796D554 FOREIGN KEY (merchant_id) REFERENCES merchant (id)');
        $this->addSql('ALTER TABLE product_search ADD CONSTRAINT FK_D68C9A034584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product_search ADD CONSTRAINT FK_D68C9A03B09B908 FOREIGN KEY (quantity_unit_id) REFERENCES quantity_unit (id)');
        $this->addSql('ALTER TABLE sell_proposal ADD CONSTRAINT FK_5E82686D8DE820D9 FOREIGN KEY (seller_id) REFERENCES merchant (id)');
        $this->addSql('ALTER TABLE sell_proposal ADD CONSTRAINT FK_5E82686DBA7D624 FOREIGN KEY (product_search_id) REFERENCES product_search (id)');
        $this->addSql('ALTER TABLE sell_proposal ADD CONSTRAINT FK_5E82686DB09B908 FOREIGN KEY (quantity_unit_id) REFERENCES quantity_unit (id)');
        $this->addSql('ALTER TABLE transport_proposal ADD CONSTRAINT FK_75CB81384F335C8B FOREIGN KEY (transporter_id) REFERENCES transporter (id)');
        $this->addSql('ALTER TABLE transport_proposal ADD CONSTRAINT FK_75CB8138F3C4E632 FOREIGN KEY (transport_search_id) REFERENCES transport_search (id)');
        $this->addSql('ALTER TABLE transport_proposal ADD CONSTRAINT FK_75CB813888DC9EBE FOREIGN KEY (sell_proposal_id) REFERENCES sell_proposal (id)');
        $this->addSql('ALTER TABLE transport_search ADD CONSTRAINT FK_70BAE2E88DC9EBE FOREIGN KEY (sell_proposal_id) REFERENCES sell_proposal (id)');
        $this->addSql('ALTER TABLE transporter ADD CONSTRAINT FK_A036E2D4A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE transporter ADD CONSTRAINT FK_A036E2D4C52E0EE9 FOREIGN KEY (residence_city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE transporter ADD CONSTRAINT FK_A036E2D45E67DA8D FOREIGN KEY (work_city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE transporter_city ADD CONSTRAINT FK_FE9C07CE4F335C8B FOREIGN KEY (transporter_id) REFERENCES transporter (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE transporter_city ADD CONSTRAINT FK_FE9C07CE8BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_notification ADD CONSTRAINT FK_3F980AC8A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_notification ADD CONSTRAINT FK_3F980AC8EF1A9D84 FOREIGN KEY (notification_id) REFERENCES notification (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE merchant DROP FOREIGN KEY FK_74AB25E18BAC62AF');
        $this->addSql('ALTER TABLE transporter DROP FOREIGN KEY FK_A036E2D4C52E0EE9');
        $this->addSql('ALTER TABLE transporter DROP FOREIGN KEY FK_A036E2D45E67DA8D');
        $this->addSql('ALTER TABLE transporter_city DROP FOREIGN KEY FK_FE9C07CE8BAC62AF');
        $this->addSql('ALTER TABLE merchant_product_category DROP FOREIGN KEY FK_ACE1A3A96796D554');
        $this->addSql('ALTER TABLE product_search DROP FOREIGN KEY FK_D68C9A036796D554');
        $this->addSql('ALTER TABLE sell_proposal DROP FOREIGN KEY FK_5E82686D8DE820D9');
        $this->addSql('ALTER TABLE user_notification DROP FOREIGN KEY FK_3F980AC8EF1A9D84');
        $this->addSql('ALTER TABLE product_search DROP FOREIGN KEY FK_D68C9A034584665A');
        $this->addSql('ALTER TABLE merchant_product_category DROP FOREIGN KEY FK_ACE1A3A9BE6903FD');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD12469DE2');
        $this->addSql('ALTER TABLE product_category DROP FOREIGN KEY FK_CDFC7356727ACA70');
        $this->addSql('ALTER TABLE sell_proposal DROP FOREIGN KEY FK_5E82686DBA7D624');
        $this->addSql('ALTER TABLE product_search DROP FOREIGN KEY FK_D68C9A03B09B908');
        $this->addSql('ALTER TABLE sell_proposal DROP FOREIGN KEY FK_5E82686DB09B908');
        $this->addSql('ALTER TABLE transport_proposal DROP FOREIGN KEY FK_75CB813888DC9EBE');
        $this->addSql('ALTER TABLE transport_search DROP FOREIGN KEY FK_70BAE2E88DC9EBE');
        $this->addSql('ALTER TABLE transport_proposal DROP FOREIGN KEY FK_75CB8138F3C4E632');
        $this->addSql('ALTER TABLE transport_proposal DROP FOREIGN KEY FK_75CB81384F335C8B');
        $this->addSql('ALTER TABLE transporter_city DROP FOREIGN KEY FK_FE9C07CE4F335C8B');
        $this->addSql('ALTER TABLE merchant DROP FOREIGN KEY FK_74AB25E1A76ED395');
        $this->addSql('ALTER TABLE transporter DROP FOREIGN KEY FK_A036E2D4A76ED395');
        $this->addSql('ALTER TABLE user_notification DROP FOREIGN KEY FK_3F980AC8A76ED395');
        $this->addSql('DROP TABLE city');
        $this->addSql('DROP TABLE merchant');
        $this->addSql('DROP TABLE merchant_product_category');
        $this->addSql('DROP TABLE notification');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_category');
        $this->addSql('DROP TABLE product_search');
        $this->addSql('DROP TABLE quantity_unit');
        $this->addSql('DROP TABLE sell_proposal');
        $this->addSql('DROP TABLE transport_proposal');
        $this->addSql('DROP TABLE transport_search');
        $this->addSql('DROP TABLE transporter');
        $this->addSql('DROP TABLE transporter_city');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_notification');
    }
}

<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RegistrationControllerTest extends WebTestCase
{
    public function testRegisterAction()
    {
        $client = static::createClient();
        $client->request('GET', '/inscription');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}

<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityControllerTest extends WebTestCase
{
    public function testLogin()
    {
        $client = static::createClient();
        $client->request('GET', '/connexion');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testForgottenPassword()
    {
        $client = static::createClient();
        $client->request('GET', '/motdepasse/perdu');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}

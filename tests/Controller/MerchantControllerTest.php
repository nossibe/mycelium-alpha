<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MerchantControllerTest extends WebTestCase
{
    public function testList()
    {
        $client = static::createClient();
        $client->request('GET', '/liste-marchands');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}

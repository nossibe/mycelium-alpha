/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';

import('./css/global.scss');

const $ = require('jquery');
global.$ = global.jQuery = $;

require('./js/bootstrap.bundle.min.js');
require('jquery.easing/jquery.easing.min.js');

require('@fortawesome/fontawesome-free/css/all.css');
require('startbootstrap-sb-admin-2/css/sb-admin-2.min.css');
require('startbootstrap-sb-admin-2/js/sb-admin-2.min');
require('jquery-datetimepicker/build/jquery.datetimepicker.full.min.js');
require('jquery-datetimepicker/build/jquery.datetimepicker.min.css');
require('vicopo/api.min');
require('./js/images.js');
require('./js/dateTimePicker.js');
require('./js/quantityUpDown.js');
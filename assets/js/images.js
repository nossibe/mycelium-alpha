
window.onload = () => {
    let deleteLinks = document.querySelectorAll("[data-delete]");
    let messageImageDiv = document.getElementById('messageImage');

    for(deleteLink of deleteLinks){
        deleteLink.addEventListener("click", function(e){
            e.preventDefault()

            if(confirm("Voulez-vous supprimer cette image ?")){
                fetch(this.getAttribute("href"), {
                    method: "DELETE",
                    headers: {
                        "X-Requested-With": "XMLHttpRequest",
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({"_token": this.dataset.token})
                }).then(
                    response => response.json()
                ).then(data => {
                    if(data.success){
                        messageImageDiv.innerHTML = "";

                        let rowImageFile = document.getElementById('imageFile');
                        rowImageFile.classList.remove('d-none');
                        rowImageFile.classList.add('col-6');
                    }
                    else{
                        alert(data.error)
                    }
                }).catch(e => alert(e))
            }
        })
    }

    let imageInput = document.getElementById('product_search_product_imageFile');
    if (imageInput == null){
        imageInput = document.getElementById('merchant_imageFile');
    }
    if (imageInput){
        imageInput.addEventListener("change", function (e){
            messageImageDiv.innerHTML = e.target.files[0].name;
            messageImageDiv.classList.add('text-success');
        })
    }
}
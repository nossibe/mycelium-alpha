
// https://xdsoft.net/jqplugins/datetimepicker/
jQuery.datetimepicker.setLocale('fr');
jQuery('#product_search_deadLine').datetimepicker({
    defaultDate:new Date(),
    minDate: 0,
    step:30,
    format: 'd-m-Y H:i'
});
# Bienvenue dans le projet Mycelium 

![Mycelium Digital](http://mycelium.cayoo.fr/build/titre-sombre-700x228.png)


## Version Alpha 

0.1.7
* Add : Tests Security-checker + phpcs + phpstan + twig-lint

0.1.6
* Fix : Bad logout route
* Fix : first_notifications and count_notifications forgotten on connecting page
* Fix : Notifications without messages
* Fix : Test if user is already connected on connecting page
* Fix : Removing test information from notifications
* Fix : Sort users by id desc on admin users list
* Fix : Sort proposals by id desc on admin proposals list

0.1.5
* Fix : Double message alert on delivering product
* Fix : Mail titles errors

0.1.4
* Add : Forgot password

0.1.3
* Add : Email validation of registration

0.1.2
* Fix : EntityMannager Interface injected in UserNotificationSubscriber

0.1.1
* Add : Regex password (8 characters + numbers + lowercase letter + uppercase letter)
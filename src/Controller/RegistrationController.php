<?php

namespace App\Controller;

use App\Form\UserType;
use App\Entity\User;
use App\Repository\UserNotificationRepository;
use App\Repository\UserRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RegistrationController extends AbstractController
{

    /**
     * @Route("/inscription", name="user_registration")
     * @param  Request                      $request
     * @param  UserPasswordEncoderInterface $passwordEncoder
     * @param  TranslatorInterface          $translator
     * @param  UserNotificationRepository   $userNotificationRepository
     * @param  TokenGeneratorInterface      $tokenGenerator
     * @param  MailerInterface              $mailer
     * @param  UrlGeneratorInterface        $urlGenerator
     * @return RedirectResponse|Response
     * @throws TransportExceptionInterface
     */
    public function registerAction(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        TranslatorInterface $translator,
        UserNotificationRepository $userNotificationRepository,
        TokenGeneratorInterface $tokenGenerator,
        MailerInterface $mailer,
        UrlGeneratorInterface $urlGenerator
    ) {
        //TODO get notification only if connected user (if admin add user)
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(['user' => $currentUser], ['id' => 'DESC'], '5');
        $user = new User();
        $roles[] = 'ROLE_USER';
        $form = $this->createForm(
            UserType::class,
            $user,
            [
                'validation_groups' => array('User', 'registration'),
                'current_user' => $currentUser
            ]
        );
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $token = $tokenGenerator->generateToken();
            $user->setPassword($password);
            $user->setRoles($roles);
            $user->setPublished(true);
            $user->setActivationToken($token);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash(
                'success',
                $translator->trans('Your account has been created, an activation email has been sent to you.')
            );

            $context = [
                'context_link' => $urlGenerator->generate(
                    'activation',
                    [
                        'token' => $user->getActivationToken()
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                )
            ];

            $email = (new TemplatedEmail())
                ->from('no-reply@mycelium.cayoo.fr')
                ->to($user->getEmail())
                ->subject($translator->trans("Registration validation"))
                ->textTemplate('emails/Registration/activation.txt.twig')
                ->context($context)
                ->htmlTemplate('emails/Registration/activation.html.twig');

            $email->getHeaders()->addTextHeader('X-Auto-Response-Suppress', 'OOF, DR, RN, NRN, AutoReply');

            $mailer->send($email);

            return $this->redirectToRoute('profile-choice');
        }

        return $this->render(
            'registration/register.html.twig',
            [
                'current_user'        => $currentUser,
                'form'                => $form->createView(),
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }


    /**
     * @Route("/activation/{token}", name="activation")
     * @param  $token
     * @param  UserRepository      $userRepository
     * @param  TranslatorInterface $translator
     * @return RedirectResponse
     */
    public function activation(
        $token,
        UserRepository $userRepository,
        TranslatorInterface $translator
    ): RedirectResponse {
        $user = $userRepository->findOneBy(['activationToken' => $token]);

        if (!$user) {
            $this->addFlash(
                'danger',
                $translator->trans('The user do not exist.')
            );
            return $this->redirectToRoute('login');
        }

        $user->setActivationToken(null);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        $this->addFlash(
            'success',
            $translator->trans('Your account has been activated.')
        );

        return $this->redirectToRoute('profile-choice');
    }
}

<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\NewPasswordType;
use App\Form\ResetPasswordType;
use App\Repository\UserRepository;
use Exception;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/connexion", name="login")
     * @param  AuthenticationUtils $authenticationUtils
     * @param  TranslatorInterface $translator
     * @return Response
     */
    public function login(
        AuthenticationUtils $authenticationUtils,
        TranslatorInterface $translator
    ): Response {

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        $currentUser = $this->getUser();

        if ($currentUser instanceof User) {
            $this->addFlash(
                'success',
                $translator->trans('You are already connected')
            );
            return $this->redirectToRoute("home");
        } else {
            return $this->render(
                'security/login.html.twig',
                [
                    'last_username' => $lastUsername,
                    'error' => $error,
                    'first_notifications' => null,
                    'count_notifications' => null
                ]
            );
        }
    }

    /**
     * @Route("/deconnexion", name="logout")
     * @throws Exception
     */
    public function logout()
    {
        throw new Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }

    /**
     * @Route("/motdepasse/perdu", name="forgotten_password")
     * @param  Request                 $request
     * @param  UserRepository          $userRepository
     * @param  MailerInterface         $mailer
     * @param  TokenGeneratorInterface $tokenGenerator
     * @param  TranslatorInterface     $translator
     * @param  UrlGeneratorInterface   $urlGenerator
     * @return Response
     * @throws TransportExceptionInterface
     */
    public function forgottenPassword(
        Request $request,
        UserRepository $userRepository,
        MailerInterface $mailer,
        TokenGeneratorInterface $tokenGenerator,
        TranslatorInterface $translator,
        UrlGeneratorInterface $urlGenerator
    ): Response {
        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $userRepository->findOneBy(['email' => $data['email']]);

            if (!$user) {
                $this->addFlash(
                    'danger',
                    $translator->trans('The user do not exist.')
                );
                return $this->redirectToRoute('forgotten_password');
            }

            $token = $tokenGenerator->generateToken();
            try {
                $user->setResetToken($token);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();
            } catch (\Exception $e) {
                $this->addFlash('warning', $e->getMessage());
                return $this->redirectToRoute('login');
            }

            $context = [
                'context_link' => $urlGenerator->generate(
                    'modify_password',
                    [
                        'token' => $token
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                )
            ];

            $email = (new TemplatedEmail())
                ->from('no-reply@mycelium.cayoo.fr')
                ->to($user->getEmail())
                ->subject($translator->trans("Change password"))
                ->textTemplate('emails/Registration/resetPassword.txt.twig')
                ->context($context)
                ->htmlTemplate('emails/Registration/resetPassword.html.twig');

            $email->getHeaders()->addTextHeader('X-Auto-Response-Suppress', 'OOF, DR, RN, NRN, AutoReply');

            $mailer->send($email);

            $this->addFlash(
                'success',
                $translator->trans(
                    'An email has been sent to you. Click on the link in this email to change your password.'
                )
            );

            return $this->redirectToRoute('login');
        }

        return $this->render(
            'security/lostPassword.html.twig',
            [
                'emailForm'           => $form->createView(),
                'first_notifications' => null,
                'count_notifications' => null
            ]
        );
    }

    /**
     * @Route("/modifier-mot-de-passe/{token}", name="modify_password")
     * @param  $token
     * @param  Request                      $request
     * @param  UserRepository               $userRepository
     * @param  TranslatorInterface          $translator
     * @param  UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function newPassword(
        $token,
        Request $request,
        UserRepository $userRepository,
        TranslatorInterface $translator,
        UserPasswordEncoderInterface $passwordEncoder
    ): Response {

        $user = $userRepository->findOneBy(['resetToken' => $token]);
        $form = $this->createForm(
            NewPasswordType::class,
            $user
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            if (!$user) {
                $this->addFlash(
                    'danger',
                    $translator->trans('This password reset link is no longer valid.')
                );
                return $this->redirectToRoute('forgotten_password');
            }

            $password = $passwordEncoder->encodePassword($user, $data->getPlainPassword());
            $user->setPassword($password);
            $user->setResetToken(null);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash(
                'success',
                $translator->trans('Your password has been changed, you can now use it to login.')
            );

            return $this->redirectToRoute('login');
        }

        return $this->render(
            'security/newPassword.html.twig',
            [
                'newPasswordForm'     => $form->createView(),
                'first_notifications' => null,
                'count_notifications' => null
            ]
        );
    }
}

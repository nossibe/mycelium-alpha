<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Repository\UserNotificationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/admin/utilisateurs", name="admin_users_list")
     * @param  UserRepository             $repository
     * @param  UserNotificationRepository $userNotificationRepository
     * @return Response
     */
    public function list(UserRepository $repository, UserNotificationRepository $userNotificationRepository): Response
    {
        $allUsers = $repository->findBy([], ['id' => 'DESC']);

        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(['user' => $currentUser], ['id' => 'DESC'], '5');

        return $this->render(
            'admin/user/users.html.twig',
            [
                'current_menu'        => 'admin_users_list',
                'users'               => $allUsers,
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/admin/modifier-utilisateur/{user}", name="user_edit")
     * @IsGranted("EDIT_USER", subject="user")
     * @param  User                       $user
     * @param  Request                    $request
     * @param  TranslatorInterface        $translator
     * @param  UserNotificationRepository $userNotificationRepository
     * @return Response
     */
    public function edit(
        User $user,
        Request $request,
        TranslatorInterface $translator,
        UserNotificationRepository $userNotificationRepository
    ): Response {
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(['user' => $currentUser], ['id' => 'DESC'], '5');

        $form = $this->createForm(
            UserType::class,
            $user,
            [
                'validation_groups' => array('User', 'edition'),
                'current_user'      => $currentUser
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash(
                'success',
                $translator->trans('The user has been successfully modified.')
            );
            return $this->redirectToRoute('admin_users_list');
        }

        return $this->render(
            'user/edit.html.twig',
            [
                'current_menu'        => 'admin_users_list',
                'user'                => $user,
                'current_user'        => $currentUser,
                'form'                => $form->createView(),
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/membre/modifier-mon-compte", name="my_account_edit")
     * @param  Request                    $request
     * @param  TranslatorInterface        $translator
     * @param  UserNotificationRepository $userNotificationRepository
     * @return Response
     */
    public function myAccount(
        Request $request,
        TranslatorInterface $translator,
        UserNotificationRepository $userNotificationRepository
    ): Response {
        $user = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($user);
        $firstNotifications = $userNotificationRepository->findBy(['user' => $user], ['id' => 'DESC'], '5');

        $form = $this->createForm(
            UserType::class,
            $user,
            [
                'validation_groups' => array('User', 'edition'),
                'current_user'      => $user
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash(
                'success',
                $translator->trans('The user has been successfully modified.')
            );
            return $this->redirectToRoute('my_account_edit');
        }

        return $this->render(
            'user/edit.html.twig',
            [
                'current_menu'        => 'admin_users_list',
                'user'                => $user,
                'current_user'        => $user,
                'form'                => $form->createView(),
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }


    /**
     * @Route("/admin/user/edit/{id}", name="admin_user_delete", methods="DELETE")
     * @param  User                $user
     * @param  Request             $request
     * @param  TranslatorInterface $translator
     * @return Response
     */
    public function delete(User $user, Request $request, TranslatorInterface $translator): Response
    {
        if ($this->isCsrfTokenValid('deleteUser' . $user->getId(), $request->get('_token'))) {
            $this->em->remove($user);
            $this->em->flush();
            $this->addFlash(
                'warning',
                $translator->trans('The user has been deleted.')
            );
        }
        return $this->redirectToRoute('admin_users_list');
    }
}

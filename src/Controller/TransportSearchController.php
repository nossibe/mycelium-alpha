<?php

namespace App\Controller;

use App\Entity\Notification;
use App\Entity\SellProposal;
use App\Entity\TransportSearch;
use App\Entity\UserNotification;
use App\Repository\TransporterRepository;
use App\Repository\TransportProposalRepository;
use App\Repository\TransportSearchRepository;
use App\Repository\UserNotificationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class TransportSearchController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/recherche-transporteur/proposition-{proposition}", name="transport_search_add")
     * @param  SellProposal          $proposition
     * @param  TranslatorInterface   $translator
     * @param  TransporterRepository $transporterRepository
     * @return RedirectResponse|Response
     */
    public function new(
        SellProposal $proposition,
        TranslatorInterface $translator,
        TransporterRepository $transporterRepository
    ) {
        $currentUser = $this->getUser();
        $buyer = $proposition->getProductSearch()->getMerchant()->getUser();
        if (in_array('ROLE_ADMIN', $currentUser->getRoles()) or $currentUser === $buyer) {
            $transportSearch = new TransportSearch();
            $transportSearch->setSellProposal($proposition);

            $this->em->persist($transportSearch);
            $this->em->flush();
            $this->addFlash(
                'success',
                $translator->trans('The transporter Search has been added')
            );

            $this->em->refresh($transportSearch); // to get $transportProposal id after flush

            $notification = new Notification(__FUNCTION__, $transportSearch);
            $this->em->persist($notification);
            $this->em->flush();
            $this->em->refresh($notification); // to get $notification id after flush

            $sellerCity = $transportSearch->getSellProposal()->getSeller()->getCity();
            $buyerCity = $transportSearch->getSellProposal()->getProductSearch()->getMerchant()->getCity();
            $followersSellerCity = $transporterRepository->findCitiesServedFollowers($sellerCity);
            $followersBuyerCity = $transporterRepository->findCitiesServedFollowers($buyerCity);
            $searchFollowers = [];
            foreach ($followersSellerCity as $transporter) {
                if (in_array($transporter, $followersBuyerCity)) {
                    array_push($searchFollowers, $transporter);
                }
            }
            foreach ($searchFollowers as $transporterFollower) {
                $userNotification = new UserNotification();
                $userNotification->setUser($transporterFollower->getUser());
                $userNotification->setNotification($notification);
                $this->em->persist($userNotification);
            }
            $this->em->flush();

            return $this->redirectToRoute('my_product_searches_list');
        } else {
            $this->addFlash(
                'warning',
                $translator->trans('Sorry, you can not do this action')
            );
        }
    }

    /**
     * @Route("/transporteur/tous-les-transports", name="transporters_search_list")
     * @param  TransportSearchRepository   $repository
     * @param  UserNotificationRepository  $userNotificationRepository
     * @param  TransporterRepository       $transporterRepository
     * @param  TransportProposalRepository $transportProposalRepository
     * @param  PaginatorInterface          $paginator
     * @param  Request                     $request
     * @return Response
     */
    public function list(
        TransportSearchRepository $repository,
        UserNotificationRepository $userNotificationRepository,
        TransporterRepository $transporterRepository,
        TransportProposalRepository $transportProposalRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(
            ['user' => $currentUser],
            ['id' => 'DESC'],
            '5'
        );

        $myTransporterSearchesAnswered = $transportProposalRepository->findTransporterSearchesIAnswered($currentUser);
        $transportSearchClosed = $repository->findTransportSearchesClosed();
        $myTransportProposals = $transportProposalRepository->findMyTransportProposals($currentUser);
        $myCitiesServed = $transporterRepository->findMyCitiesServed($currentUser);
        $allTransportSearches = $repository->findBy(
            ['published' => 1],
            ['id' => "DESC"]
        );

        $allTransportSearchesPaginate = $paginator->paginate(
            $allTransportSearches,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render(
            'transportSearch/searches.html.twig',
            [
                'current_menu'                     => 'transporters_search_list',
                'transport_searches'               => $allTransportSearchesPaginate,
                'my_cities_served'                 => $myCitiesServed,
                'my_transporter_searches_answered' => $myTransporterSearchesAnswered,
                'my_transport_proposals'           => $myTransportProposals,
                'transport_search_closed'          => $transportSearchClosed,
                'first_notifications'              => $firstNotifications,
                'count_notifications'              => $countNotifications
            ]
        );
    }

    /**
     * @Route("/transporteur/transports-sur-ma-route", name="my_transport_search_list")
     * @param  TransportSearchRepository   $repository
     * @param  UserNotificationRepository  $userNotificationRepository
     * @param  TransporterRepository       $transporterRepository
     * @param  TransportProposalRepository $transportProposalRepository
     * @param  PaginatorInterface          $paginator
     * @param  Request                     $request
     * @return Response
     */
    public function myList(
        TransportSearchRepository $repository,
        UserNotificationRepository $userNotificationRepository,
        TransporterRepository $transporterRepository,
        TransportProposalRepository $transportProposalRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $allTransportSearches = $repository->findBy(
            ['published' => 1],
            ['id' => "DESC"]
        );
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(
            ['user' => $currentUser],
            ['id' => 'DESC'],
            '5'
        );

        $transportSearchClosed = $repository->findTransportSearchesClosed();
        $myTransporterSearchesAnswered = $transportProposalRepository->findTransporterSearchesIAnswered($currentUser);
        $myTransportProposals = $transportProposalRepository->findMyTransportProposals($currentUser);
        $myCitiesServed = $transporterRepository->findMyCitiesServed($currentUser);
        $transportSearchOnRoad = [];

        foreach ($allTransportSearches as $transportSearch) {
            $sellerCity = $transportSearch->getSellProposal()->getSeller()->getCity()->getId();
            $buyerCity = $transportSearch->getSellProposal()->getProductSearch()->getMerchant()->getCity()->getId();
            if (in_array($sellerCity, $myCitiesServed) and in_array($buyerCity, $myCitiesServed)) {
                $transportSearchOnRoad[] = $transportSearch;
            }
        }

        $allTransportSearchesPaginate = $paginator->paginate(
            $transportSearchOnRoad,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render(
            'transportSearch/searches.html.twig',
            [
                'current_menu'                     => 'my_transport_search_list',
                'transport_searches'               => $allTransportSearchesPaginate,
                'my_cities_served'                 => $myCitiesServed,
                'my_transporter_searches_answered' => $myTransporterSearchesAnswered,
                'my_transport_proposals'           => $myTransportProposals,
                'transport_search_closed'          => $transportSearchClosed,
                'first_notifications'              => $firstNotifications,
                'count_notifications'              => $countNotifications
            ]
        );
    }

    /**
     * @Route("/transporteur/mes-reponses", name="transport_list_i_answered")
     * @param  TransportSearchRepository   $repository
     * @param  UserNotificationRepository  $userNotificationRepository
     * @param  TransporterRepository       $transporterRepository
     * @param  TransportProposalRepository $transportProposalRepository
     * @param  PaginatorInterface          $paginator
     * @param  Request                     $request
     * @return Response
     */
    public function listIAnswered(
        TransportSearchRepository $repository,
        UserNotificationRepository $userNotificationRepository,
        TransporterRepository $transporterRepository,
        TransportProposalRepository $transportProposalRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(
            ['user' => $currentUser],
            ['id' => 'DESC'],
            '5'
        );

        $myTransporterSearchesAnswered = $transportProposalRepository->findTransporterSearchesIAnswered($currentUser);
        $transportSearchClosed = $repository->findTransportSearchesClosed();
        $myTransportProposals = $transportProposalRepository->findMyTransportProposals($currentUser);
        $myCitiesServed = $transporterRepository->findMyCitiesServed($currentUser);

        $allTransportSearchesPaginate = $paginator->paginate(
            $myTransporterSearchesAnswered,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render(
            'transportSearch/searches.html.twig',
            [
                'current_menu'                     => 'transport_list_i_answered',
                'transport_searches'               => $allTransportSearchesPaginate,
                'my_transporter_searches_answered' => $myTransporterSearchesAnswered,
                'my_cities_served'                 => $myCitiesServed,
                'my_transport_proposals'           => $myTransportProposals,
                'transport_search_closed'          => $transportSearchClosed,
                'first_notifications'              => $firstNotifications,
                'count_notifications'              => $countNotifications
            ]
        );
    }
}

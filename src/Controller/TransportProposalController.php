<?php

namespace App\Controller;

use App\Entity\Notification;
use App\Entity\TransportProposal;
use App\Entity\TransportSearch;
use App\Entity\UserNotification;
use App\Form\TransportProposalType;
use App\Repository\TransporterRepository;
use App\Repository\TransportProposalRepository;
use App\Repository\UserNotificationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class TransportProposalController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/transporteur/proposition-transport-{transportSearch}", name="transport_proposal_add")
     * @param  TransportSearch            $transportSearch
     * @param  Request                    $request
     * @param  TranslatorInterface        $translator
     * @param  UserNotificationRepository $userNotificationRepository
     * @param  TransporterRepository      $transporterRepository
     * @return RedirectResponse|Response
     */
    public function new(
        TransportSearch $transportSearch,
        Request $request,
        TranslatorInterface $translator,
        UserNotificationRepository $userNotificationRepository,
        TransporterRepository $transporterRepository
    ) {
        $transportProposal = new TransportProposal();
        $currentUser = $this->getUser();
        $currentTransporter = $transporterRepository->findOneBy(['user' => $currentUser]);
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(['user' => $currentUser], ['id' => 'DESC'], '5');

        $transportProposal->setPublished('1');
        $transportProposal->setTransporter($currentTransporter);
        $transportProposal->setTransportSearch($transportSearch);
        $transportProposal->setSellProposal($transportSearch->getSellProposal());
        $transportProposal->setBuyerAgreement(false);

        $form = $this->createForm(
            TransportProposalType::class,
            $transportProposal,
            [
                'current_user' => $currentUser
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($transportProposal);
            $this->em->flush();
            $this->addFlash(
                'success',
                $translator->trans('The transport proposal has been sent to the merchant who launched the search.')
            );

            $this->em->refresh($transportProposal); // to get $transportProposal id after flush

            $notification = new Notification(__FUNCTION__, $transportProposal);
            $this->em->persist($notification);
            $this->em->flush();
            $this->em->refresh($notification); // to get $notification id after flush*/

            $sellerBuyerMerchants[] = $transportProposal->getSellProposal()->getSeller();
            $sellerBuyerMerchants[] = $transportProposal->getSellProposal()->getProductSearch()->getMerchant();

            foreach ($sellerBuyerMerchants as $merchantToNotify) {
                $userNotification = new UserNotification();
                $userNotification->setUser($merchantToNotify->getUser());
                $userNotification->setNotification($notification);
                $this->em->persist($userNotification);
            }
            $this->em->flush();

            return $this->redirectToRoute('transport_list_i_answered');
        }

        return $this->render(
            'transportProposal/new.html.twig',
            [
                'current_menu'        => 'transport_proposal_list',
                'current_user'        => $currentUser,
                'transportProposal'   => $transportProposal,
                'transport_search'    => $transportSearch,
                'form'                => $form->createView(),
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/marchand/propositions-transports", name="transport_proposal_list")
     * @param  TransportProposalRepository $repository
     * @param  UserNotificationRepository  $userNotificationRepository
     * @return Response
     */
    public function list(
        TransportProposalRepository $repository,
        UserNotificationRepository $userNotificationRepository
    ): Response {
        $allTransportProposals = $repository->findAll();
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(['user' => $currentUser], ['id' => 'DESC'], '5');

        return $this->render(
            'transportProposal/transportProposals.html.twig',
            [
                'current_menu'        => 'transport_proposal_list',
                'transportProposals'  => $allTransportProposals,
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/transporter/desactiver-proposition-{transportProposal}", name="disable_transport_proposal")
     * @param  TransportProposal   $transportProposal
     * @param  TranslatorInterface $translator
     * @return RedirectResponse
     */
    public function disable(TransportProposal $transportProposal, TranslatorInterface $translator): RedirectResponse
    {
        $currentUser = $this->getUser();
        $transporter = $transportProposal->getTransporter()->getUser();
        if (in_array('ROLE_ADMIN', $currentUser->getRoles()) or $currentUser == $transporter) {
            $transportProposal->setPublished("0");

            $this->em->persist($transportProposal);
            $this->em->flush();
            $this->addFlash(
                'success',
                $translator->trans('Transport proposal has been successfully disabled')
            );
        } else {
            $this->addFlash(
                'warning',
                $translator->trans('Sorry, you can not do this action')
            );
        }
        return $this->redirectToRoute('transport_list_i_answered');
    }

    /**
     * @Route("/transporteur/activer-proposition-{transportProposal}", name="enable_transport_proposal")
     * @param  TransportProposal   $transportProposal
     * @param  TranslatorInterface $translator
     * @return RedirectResponse
     */
    public function enable(
        TransportProposal $transportProposal,
        TranslatorInterface $translator
    ): RedirectResponse {
        $currentUser = $this->getUser();
        $transporter = $transportProposal->getTransporter()->getUser();
        if (in_array('ROLE_ADMIN', $currentUser->getRoles()) or $currentUser == $transporter) {
            $transportProposal->setPublished("1");

            $this->em->persist($transportProposal);
            $this->em->flush();
            $this->addFlash(
                'success',
                $translator->trans('Transport proposal has been successfully enabled')
            );
        } else {
            $this->addFlash(
                'warning',
                $translator->trans('Sorry, you can not do this action')
            );
        }
        return $this->redirectToRoute('transport_list_i_answered');
    }

    /**
     * @Route("/marchand/accepter-transport-{transportProposal}", name="agree_transport_proposal")
     * @param  TransportProposal   $transportProposal
     * @param  TranslatorInterface $translator
     * @return RedirectResponse
     */
    public function buyerAgree(
        TransportProposal $transportProposal,
        TranslatorInterface $translator
    ): RedirectResponse {
        $currentUser = $this->getUser();
        $buyer = $transportProposal->getSellProposal()->getProductSearch()->getMerchant()->getUser();
        $productSearch = $transportProposal->getSellProposal()->getProductSearch();

        if (in_array('ROLE_ADMIN', $currentUser->getRoles()) or $currentUser == $buyer) {
            $productSearch->setClosedDateTime(new \DateTime());
            $this->em->persist($productSearch);

            $transportProposal->setBuyerAgreement("1");

            $this->em->persist($transportProposal);
            $this->em->flush();
            $this->addFlash(
                'success',
                $translator->trans('You have accepted this transport. Consequently your product search is deactivated.')
            );

            $notification = new Notification(__FUNCTION__, $transportProposal);
            $this->em->persist($notification);
            $this->em->flush();
            $this->em->refresh($notification);

            $sellerTransporter[] = $transportProposal->getSellProposal()->getSeller();
            $sellerTransporter[] = $transportProposal->getTransporter();

            foreach ($sellerTransporter as $userToNotify) {
                $userNotification = new UserNotification();
                $userNotification->setUser($userToNotify->getUser());
                $userNotification->setNotification($notification);
                $this->em->persist($userNotification);
            }
            $this->em->flush();
        } else {
            $this->addFlash(
                'warning',
                $translator->trans('Sorry, you can not agree a transport for this research')
            );
        }
        return $this->redirectToRoute(
            'show_proposal',
            ['sellProposal' => $transportProposal->getSellProposal()->getId()]
        );
    }

    /**
     * @Route("/proposition-transport/supprimer/{id}", name="transport_proposal_delete", methods="DELETE")
     * @param  TransportProposal   $transportProposal
     * @param  Request             $request
     * @param  TranslatorInterface $translator
     * @return Response
     */
    public function delete(
        TransportProposal $transportProposal,
        Request $request,
        TranslatorInterface $translator
    ): Response {
        if ($transportProposal->getBuyerAgreement() == null) {
            $currentUser = $this->getUser();
            $transporter = $transportProposal->getTransporter()->getUser();
            if (in_array('ROLE_ADMIN', $currentUser->getRoles()) or $currentUser == $transporter) {
                if (
                    $this->isCsrfTokenValid(
                        'deleteTransportProposal' . $transportProposal->getId(),
                        $request->get('_token')
                    )
                ) {
                    $notification = new Notification(__FUNCTION__, $transportProposal);
                    $this->em->persist($notification);
                    $this->em->flush();
                    $this->em->refresh($notification);

                    $buyer = $transportProposal->getSellProposal()->getProductSearch()->getMerchant()->getUser();

                    $userNotification = new UserNotification();
                    $userNotification->setUser($buyer);
                    $userNotification->setNotification($notification);
                    $this->em->persist($userNotification);
                    $this->em->flush();

                    $this->em->remove($transportProposal);
                    $this->em->flush();
                    $this->addFlash(
                        'warning',
                        $translator->trans('The transport proposal has been deleted')
                    );
                }
            } else {
                $this->addFlash(
                    'warning',
                    $translator->trans('You cannot delete this transport proposal')
                );
            }
        } else {
            $this->addFlash(
                'warning',
                $translator->trans('You cannot delete a transport proposal that has already been accepted')
            );
        }
        return $this->redirectToRoute('transport_list_i_answered');
    }

    /**
     * @Route("/transport-en-cours/{id}", name="transport_proposal_in_delivering")
     * @param  TransportProposal   $transportProposal
     * @param  TranslatorInterface $translator
     * @return Response
     */
    public function isInDelivering(
        TransportProposal $transportProposal,
        TranslatorInterface $translator
    ): Response {
        $currentUser = $this->getUser();
        $seller = $transportProposal->getSellProposal()->getSeller()->getUser();
        if (in_array('ROLE_ADMIN', $currentUser->getRoles()) or $currentUser == $seller) {
            $transportProposal->setInDelivering(new \DateTime());
            $this->em->persist($transportProposal);
            $this->em->flush();

            $notification = new Notification(__FUNCTION__, $transportProposal);
            $this->em->persist($notification);
            $this->em->flush();
            $this->em->refresh($notification);

            $buyer = $transportProposal->getSellProposal()->getProductSearch()->getMerchant()->getUser();

            $userNotification = new UserNotification();
            $userNotification->setUser($buyer);
            $userNotification->setNotification($notification);
            $this->em->persist($userNotification);
            $this->em->flush();
        } else {
            $this->addFlash(
                'warning',
                $translator->trans('You cannot mark this transport as in delivering')
            );
        }
        return $this->redirectToRoute(
            'show_proposal',
            [
                'sellProposal' => $transportProposal->getSellProposal()->getId()
            ]
        );
    }

    /**
     * @Route("/transport-termine/{id}", name="transport_proposal_delivered")
     * @param  TransportProposal   $transportProposal
     * @param  TranslatorInterface $translator
     * @return Response
     */
    public function isDelivered(
        TransportProposal $transportProposal,
        TranslatorInterface $translator
    ): Response {
        $currentUser = $this->getUser();
        $buyer = $transportProposal
            ->getSellProposal()
            ->getProductSearch()
            ->getMerchant()
            ->getUser();

        if (in_array('ROLE_ADMIN', $currentUser->getRoles()) or $currentUser == $buyer) {
            $transportProposal->setDelivered(new \DateTime());
            $this->em->persist($transportProposal);
            $this->em->flush();

            $notification = new Notification(__FUNCTION__, $transportProposal);
            $this->em->persist($notification);
            $this->em->flush();
            $this->em->refresh($notification);

            $seller = $transportProposal->getSellProposal()->getSeller()->getUser();

            $userNotification = new UserNotification();
            $userNotification->setUser($seller);
            $userNotification->setNotification($notification);
            $this->em->persist($userNotification);
            $this->em->flush();
        } else {
            $this->addFlash(
                'warning',
                $translator->trans('You cannot mark this transport as delivered')
            );
        }
        return $this->redirectToRoute(
            'show_proposal',
            [
                'sellProposal' => $transportProposal->getSellProposal()->getId()
            ]
        );
    }
}

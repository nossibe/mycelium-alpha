<?php

namespace App\Controller;

use App\Entity\UserNotification;
use App\Repository\TransportProposalRepository;
use App\Repository\UserNotificationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class NotificationController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param  UserNotificationRepository $userNotificationRepository
     * @param  PaginatorInterface         $paginator
     * @param  Request                    $request
     * @return Response
     * @Route("/membre/mes-notifications", name="my_notifications_list")
     */
    public function list(
        UserNotificationRepository $userNotificationRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $currentUser = $this->getUser();
        $userNotifications = $userNotificationRepository->findBy(['user' => $currentUser], ['id' => "DESC"]);
        $firstNotifications = $userNotificationRepository->findBy(['user' => $currentUser], ['id' => 'DESC'], '5');

        $notifications = $paginator->paginate(
            $userNotifications,
            $request->query->getInt('page', 1),
            5
        );

        $countNotifications = $userNotificationRepository->unreadCount($currentUser);

        return $this->render(
            'notification/notifications.html.twig',
            [
                'current_menu'        => 'my_notifications_list',
                'user_notifications'  => $userNotifications,
                'first_notifications' => $firstNotifications,
                'notifications'       => $notifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/membre/notification-lue/{userNotification}", name="mark_as_read")
     * @param  UserNotification            $userNotification
     * @param  TranslatorInterface         $translator
     * @param  TransportProposalRepository $transportProposalRepository
     * @return RedirectResponse
     */
    public function markAsRead(
        UserNotification $userNotification,
        TranslatorInterface $translator,
        TransportProposalRepository $transportProposalRepository
    ): RedirectResponse {
        $userNotification->setReadAt(new \DateTime());
        $this->em->persist($userNotification);
        $this->em->flush();

        $notification = $userNotification->getNotification();
        $objectClass = $notification->getObjectChanged();
        if ($objectClass == "ProductSearch") {
            if ($notification->getEventType() === "new") {
                return $this->redirectToRoute('product_searches_list');
            } else {
                return $this->redirectToRoute(
                    'product_search_page',
                    [
                        'productSearchId' => $notification->getObjectId()
                    ]
                );
            }
        } elseif ($objectClass == "SellProposal") {
            return $this->redirectToRoute('show_proposal', ['sellProposal' => $notification->getObjectId()]);
        } elseif ($objectClass == "TransportSearch") {
            return $this->redirectToRoute('my_transport_search_list');
        } elseif ($objectClass == "TransportProposal") {
            $currentUser = $this->getUser();
            $transportProposal = $transportProposalRepository->find($notification->getObjectId());
            if ($transportProposal->getTransporter()->getUser() == $currentUser) {
                return $this->redirectToRoute('transport_list_i_answered');
            } else {
                return $this->redirectToRoute(
                    'show_proposal',
                    [
                        'sellProposal' => $transportProposal->getSellProposal()->getId()
                    ]
                );
            }
        } else {
            $this->addFlash(
                'success',
                $translator->trans('An error occurred while viewing this notification.')
            );
            return $this->redirectToRoute('my_notifications_list');
        }
    }

    /**
     * @Route("/membre/toutes-notifications-lues", name="mark_all_as_read")
     * @param  UserNotificationRepository $userNotificationRepository
     * @return RedirectResponse
     */
    public function allMarkAsRead(UserNotificationRepository $userNotificationRepository): RedirectResponse
    {
        $currentUser = $this->getUser();
        $userNotifications = $userNotificationRepository->findBy(['user' => $currentUser, 'readAt' => null]);
        foreach ($userNotifications as $userNotification) {
            $userNotification->setReadAt(new \DateTime());
            $this->em->persist($userNotification);
        }
        $this->em->flush();
        return $this->redirectToRoute('my_notifications_list');
    }
}

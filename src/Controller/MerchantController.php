<?php

namespace App\Controller;

use App\Entity\Merchant;
use App\Form\MerchantType;
use App\Repository\MerchantRepository;
use App\Repository\UserNotificationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class MerchantController extends AbstractController
{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/membre/nouveau-marchand", name="merchant_add")
     * @param  Request                    $request
     * @param  TranslatorInterface        $translator
     * @param  UserNotificationRepository $userNotificationRepository
     * @return RedirectResponse|Response
     */
    public function new(
        Request $request,
        TranslatorInterface $translator,
        UserNotificationRepository $userNotificationRepository
    ) {
        $merchant = new Merchant();

        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(['user' => $currentUser], ['id' => 'DESC'], '5');

        $form = $this->createForm(
            MerchantType::class,
            $merchant,
            [
                'current_user' => $currentUser
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($merchant);
            $this->em->persist($merchant->getCity());
            $this->em->flush();
            $this->addFlash(
                'success',
                $translator->trans('The merchant has been added. Updating your rights requires reconnection.')
            );
            return $this->redirectToRoute('merchants_list');
        }

        return $this->render(
            'merchant/new.html.twig',
            [
                'current_menu'        => 'merchant_add',
                'merchant'            => $merchant,
                'current_user'        => $currentUser,
                'form'                => $form->createView(),
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/marchand/edit/{merchant}", name="merchant_edit")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('EDIT_MERCHANT', merchant)")
     * @param  Merchant                   $merchant
     * @param  Request                    $request
     * @param  TranslatorInterface        $translator
     * @param  UserNotificationRepository $userNotificationRepository
     * @return RedirectResponse|Response
     */
    public function edit(
        Merchant $merchant,
        Request $request,
        TranslatorInterface $translator,
        UserNotificationRepository $userNotificationRepository
    ) {
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(['user' => $currentUser], ['id' => 'DESC'], '5');

        $form = $this->createForm(
            MerchantType::class,
            $merchant,
            [
                'current_user' => $currentUser
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            if (in_array('ROLE_ADMIN', $currentUser->getRoles())) {
                $this->addFlash(
                    'success',
                    $translator->trans('The merchant has been edited. Updating your rights requires reconnection.')
                );
            } else {
                $this->addFlash(
                    'success',
                    $translator->trans('Your merchant has been modified.')
                );
            }

            return $this->redirectToRoute(
                'merchant_page',
                array(
                    'businessId' => $merchant->getId(),
                    'slug'       => $merchant->getBusinessName()
                )
            );
        }

        return $this->render(
            'merchant/edit.html.twig',
            [
                'current_menu'        => 'merchant_edit',
                'merchant'            => $merchant,
                'current_user'        => $currentUser,
                'form'                => $form->createView(),
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/liste-marchands", name="merchants_list")
     * @param  MerchantRepository         $repository
     * @param  UserNotificationRepository $userNotificationRepository
     * @param  PaginatorInterface         $paginator
     * @param  Request                    $request
     * @return Response
     */
    public function list(
        MerchantRepository $repository,
        UserNotificationRepository $userNotificationRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {

        $allMerchants = $repository->findAll();
        $merchants = $paginator->paginate(
            $allMerchants,
            $request->query->getInt('page', 1),
            10
        );

        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(['user' => $currentUser], ['id' => 'DESC'], '5');

        return $this->render(
            'merchant/merchants.html.twig',
            [
                'current_menu'        => 'merchants_list',
                'merchants'           => $merchants,
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/{businessId}-{slug}", name="merchant_page")
     * @param  MerchantRepository         $repository
     * @param  string                     $businessId
     * @param  UserNotificationRepository $userNotificationRepository
     * @return Response
     */
    public function page(
        MerchantRepository $repository,
        string $businessId,
        UserNotificationRepository $userNotificationRepository
    ): Response {
        $oneMerchant = $repository->findOneBy(['id' => $businessId]);
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(['user' => $currentUser], ['id' => 'DESC'], '5');

        return $this->render(
            'merchant/oneMerchant.html.twig',
            [
                'current_menu'        => 'merchants_list',
                'merchant'            => $oneMerchant,
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/admin/marchand/edit/{id}", name="admin_merchant_delete", methods="DELETE")
     * @param  Merchant            $merchant
     * @param  Request             $request
     * @param  TranslatorInterface $translator
     * @return Response
     */
    public function delete(Merchant $merchant, Request $request, TranslatorInterface $translator): Response
    {
        if ($this->isCsrfTokenValid('deleteMerchant' . $merchant->getId(), $request->get('_token'))) {
            $this->em->remove($merchant);
            $this->addFlash(
                'warning',
                $translator->trans('The merchant has been deleted.')
            );
            $this->em->flush();
        }
        return $this->redirectToRoute('merchants_list');
    }


    /**
     * @Route("/marchand/supprimer-image-marchand/{id}", name="merchant_delete_image", methods="DELETE")
     * @param  Merchant $merchant
     * @param  Request  $request
     * @return JsonResponse
     */
    public function deleteImage(Merchant $merchant, Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if ($this->isCsrfTokenValid($merchant->getId(), $data['_token'])) {
            $merchant->setFilename('');
            $merchant->setImageFile(null);
            $merchant->setUpdatedAt(null);

            $this->em->persist($merchant);
            $this->em->flush();

            return new JsonResponse(['success' => 1]);
        } else {
            return new JsonResponse(['error' => 'Invalid token'], 400);
        }
    }
}

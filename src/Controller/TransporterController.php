<?php

namespace App\Controller;

use App\Entity\Transporter;
use App\Entity\User;
use App\Form\TransporterType;
use App\Repository\TransporterRepository;
use App\Repository\UserNotificationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class TransporterController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/consomacteur/edit/{transporter}", name="transporter_edit")
     * @IsGranted("EDIT_TRANSPORTER",             subject="transporter")
     * @param  Transporter                $transporter
     * @param  Request                    $request
     * @param  TranslatorInterface        $translator
     * @param  UserNotificationRepository $userNotificationRepository
     * @return RedirectResponse|Response
     */
    public function edit(
        Transporter $transporter,
        Request $request,
        TranslatorInterface $translator,
        UserNotificationRepository $userNotificationRepository
    ) {
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(
            ['user' => $currentUser],
            ['id' => 'DESC'],
            '5'
        );

        $form = $this->createForm(
            TransporterType::class,
            $transporter,
            [
                'current_user' => $currentUser
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash(
                'success',
                $translator->trans(
                    'The profile Consom\'Acteur has been modified. Updating your rights requires reconnection.'
                )
            );
            return $this->redirectToRoute('transporter_edit', array( 'transporter' => $transporter->getId() ));
        }

        return $this->render(
            'transporter/edit.html.twig',
            [
                'current_menu'        => 'transporter_edit',
                'transporter'         => $transporter,
                'current_user'        => $currentUser,
                'form'                => $form->createView(),
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/membre/consomacteur/nouveau", name="transporter_add")
     * @param  Request                    $request
     * @param  TranslatorInterface        $translator
     * @param  UserNotificationRepository $userNotificationRepository
     * @return RedirectResponse|Response
     */
    public function new(
        Request $request,
        TranslatorInterface $translator,
        UserNotificationRepository $userNotificationRepository
    ) {
        $transporter = new Transporter();
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(
            ['user' => $currentUser],
            ['id' => 'DESC'],
            '5'
        );

        if ($currentUser instanceof User) {
            $transporter->setUser($currentUser);
        }

        $form = $this->createForm(
            TransporterType::class,
            $transporter,
            [
                'current_user' => $currentUser
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($transporter);
            $this->em->flush();
            $this->addFlash(
                'success',
                $translator->trans('The transporter has been created. Updating your rights requires reconnection.')
            );
            if (in_array('ROLE_ADMIN', $currentUser->getRoles())) {
                return $this->redirectToRoute('transporters_list');
            } else {
                return $this->redirectToRoute(
                    'transporter_edit',
                    [
                        'transporter' => $transporter->getId()
                    ]
                );
            }
        }

        return $this->render(
            'transporter/new.html.twig',
            [
                'current_menu'        => 'transporter_add',
                'transporter'         => $transporter,
                'current_user'        => $currentUser,
                'form'                => $form->createView(),
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/marchand/liste-transporteurs", name="transporters_list")
     * @param  TransporterRepository      $repository
     * @param  UserNotificationRepository $userNotificationRepository
     * @return Response
     */
    public function list(
        TransporterRepository $repository,
        UserNotificationRepository $userNotificationRepository
    ): Response {
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(
            ['user' => $currentUser],
            ['id' => 'DESC'],
            '5'
        );

        if (in_array('ROLE_ADMIN', $currentUser->getRoles())) {
            $allTransporters = $repository->findAll();
        } else {
            $allTransporters = $repository->findBy(['published' => 1]);
        }

        return $this->render(
            'transporter/transporters.html.twig',
            [
                'current_menu'        => 'transporters_list',
                'transporters'        => $allTransporters,
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/admin/transporter/edit/{id}", name="admin_transporter_delete", methods="DELETE")
     * @param  Transporter         $transporter
     * @param  Request             $request
     * @param  TranslatorInterface $translator
     * @return Response
     */
    public function delete(
        Transporter $transporter,
        Request $request,
        TranslatorInterface $translator
    ): Response {
        if ($this->isCsrfTokenValid('deleteTransporter' . $transporter->getId(), $request->get('_token'))) {
            $this->em->remove($transporter);
            $this->em->flush();
            $this->addFlash(
                'warning',
                $translator->trans('The carrier has been deleted')
            );
        }
        return $this->redirectToRoute('transporters_list');
    }
}

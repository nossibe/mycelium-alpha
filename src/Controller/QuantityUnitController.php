<?php

namespace App\Controller;

use App\Entity\QuantityUnit;
use App\Form\QuantityUnitType;
use App\Repository\QuantityUnitRepository;
use App\Repository\UserNotificationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class QuantityUnitController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/unite-de-quantite/liste", name="quantity_unit_list")
     * @param  QuantityUnitRepository     $repository
     * @param  UserNotificationRepository $userNotificationRepository
     * @return Response
     */
    public function list(
        QuantityUnitRepository $repository,
        UserNotificationRepository $userNotificationRepository
    ): Response {
        $allQuantityUnit = $repository->findAll();
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(
            ['user' => $currentUser],
            ['id' => 'DESC'],
            '5'
        );

        return $this->render(
            'quantity_unit/quantityUnits.html.twig',
            [
                'current_menu'        => 'quantity_unit_list',
                'quantityUnits'       => $allQuantityUnit,
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/unite-de-quantite/nouveau", name="quantity_unit_add")
     * @param  Request                    $request
     * @param  TranslatorInterface        $translator
     * @param  UserNotificationRepository $userNotificationRepository
     * @return RedirectResponse|Response
     */
    public function new(
        Request $request,
        TranslatorInterface $translator,
        UserNotificationRepository $userNotificationRepository
    ) {
        $quantityUnit = new QuantityUnit();
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(
            ['user' => $currentUser],
            ['id' => 'DESC'],
            '5'
        );

        $form = $this->createForm(
            QuantityUnitType::class,
            $quantityUnit,
            [
                'current_user' => $currentUser
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($quantityUnit);
            $this->em->flush();
            $this->addFlash(
                'success',
                $translator->trans('The unit quantity has been added.')
            );
            return $this->redirectToRoute('quantity_unit_list');
        }

        return $this->render(
            'quantity_unit/new.html.twig',
            [
                'current_menu'        => 'quantity_unit_list',
                'quantity_unit'       => $quantityUnit,
                'current_user'        => $currentUser,
                'form'                => $form->createView(),
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/unite-de-quantite/edit-unite/{quantityUnit}", name="quantity_unit_edit")
     * @Security("is_granted('ROLE_ADMIN')")
     * @param  QuantityUnit               $quantityUnit
     * @param  Request                    $request
     * @param  TranslatorInterface        $translator
     * @param  UserNotificationRepository $userNotificationRepository
     * @return RedirectResponse|Response
     */
    public function edit(
        QuantityUnit $quantityUnit,
        Request $request,
        TranslatorInterface $translator,
        UserNotificationRepository $userNotificationRepository
    ) {
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(
            ['user' => $currentUser],
            ['id' => 'DESC'],
            '5'
        );

        $form = $this->createForm(
            QuantityUnitType::class,
            $quantityUnit,
            [
                'current_user' => $currentUser
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash(
                'success',
                $translator->trans('The unit quantity has been edited.')
            );

            return $this->redirectToRoute('quantity_unit_list');
        }

        return $this->render(
            'quantity_unit/edit.html.twig',
            [
                'current_menu'        => 'quantity_unit_list',
                'quantity_unit'       => $quantityUnit,
                'current_user'        => $currentUser,
                'form'                => $form->createView(),
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/admin/unite-de-quantite/suppimer/{id}", name="quantity_unit_delete", methods="DELETE")
     * @param  QuantityUnit        $quantityUnit
     * @param  Request             $request
     * @param  TranslatorInterface $translator
     * @return Response
     */
    public function delete(
        QuantityUnit $quantityUnit,
        Request $request,
        TranslatorInterface $translator
    ): Response {
        if ($this->isCsrfTokenValid('deleteQuantityUnit' . $quantityUnit->getId(), $request->get('_token'))) {
            $this->em->remove($quantityUnit);
            $this->addFlash(
                'warning',
                $translator->trans('The unit quantity has been deleted.')
            );
            $this->em->flush();
        }
        return $this->redirectToRoute('quantity_unit_list');
    }
}

<?php

namespace App\Controller;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProductController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/marchand/supprimer-image-produit/{id}", name="product_delete_image", methods="DELETE")
     * @param  Product $product
     * @param  Request $request
     * @return JsonResponse
     */
    public function deleteImage(Product $product, Request $request): JsonResponse
    {

        $data = json_decode($request->getContent(), true);

        if ($this->isCsrfTokenValid($product->getId(), $data['_token'])) {
            $product->setFilename('');
            $product->setImageFile(null);
            $product->setUpdatedAt(null);

            $this->em->persist($product);
            $this->em->flush();

            return new JsonResponse(['success' => 1]);
        } else {
            return new JsonResponse(['error' => 'Token Invalide'], 400);
        }
    }
}

<?php

namespace App\Controller;

use App\Entity\ProductSearch;
use App\Entity\SellProposal;
use App\Entity\Notification;
use App\Entity\UserNotification;
use App\Form\SellProposalType;
use App\Repository\MerchantRepository;
use App\Repository\ProductSearchRepository;
use App\Repository\SellProposalRepository;
use App\Repository\TransportProposalRepository;
use App\Repository\TransportSearchRepository;
use App\Repository\UserNotificationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class SellProposalController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/marchand/ajout-proposition/recherche-{search}", name="sell_proposal_add")
     * @param  Request                    $request
     * @param  TranslatorInterface        $translator
     * @param  ProductSearch              $search
     * @param  UserNotificationRepository $userNotificationRepository
     * @param  ProductSearchRepository    $productSearchRepository
     * @param  MerchantRepository         $merchantRepository
     * @return RedirectResponse|Response
     */
    public function new(
        Request $request,
        TranslatorInterface $translator,
        ProductSearch $search,
        UserNotificationRepository $userNotificationRepository,
        ProductSearchRepository $productSearchRepository,
        MerchantRepository $merchantRepository
    ) {
        $sellProposal = new SellProposal();

        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(['user' => $currentUser], ['id' => 'DESC'], '5');

        $productSearched = $productSearchRepository->find($search);
        $currentUserMerchant = $merchantRepository->findOneBy(['user' => $currentUser]);

        $sellProposal->setProductSearch($productSearched);
        $sellProposal->setQuantityUnit($productSearched->getQuantityUnit());
        $sellProposal->setSeller($currentUserMerchant);


        $form = $this->createForm(
            SellProposalType::class,
            $sellProposal,
            [
                'current_user' => $currentUser
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($sellProposal);
            $this->em->flush();
            $this->addFlash(
                'success',
                $translator->trans('The proposal has been sent to the merchant who launched the search.')
            );
            $this->em->refresh($sellProposal); // to get $sellProposal id after flush

            $notification = new Notification(__FUNCTION__, $sellProposal);
            $this->em->persist($notification);
            $this->em->flush();
            $this->em->refresh($notification);

            $userNotification = new UserNotification();
            $userNotification->setUser($sellProposal->getProductSearch()->getMerchant()->getUser());
            $userNotification->setNotification($notification);
            $this->em->persist($userNotification);
            $this->em->flush();

            return $this->redirectToRoute('my_sell_proposal_list');
        }

        return $this->render(
            'sellProposal/new.html.twig',
            [
                'current_menu'        => 'sell_proposal_list',
                'sellProposal'        => $sellProposal,
                'form'                => $form->createView(),
                'productSearched'     => $productSearched,
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/marchand/modifier-proposition/{sellProposal}", name="sell_proposal_edit")
     * @param  SellProposal               $sellProposal
     * @param  Request                    $request
     * @param  TranslatorInterface        $translator
     * @param  UserNotificationRepository $userNotificationRepository
     * @return RedirectResponse|Response
     */
    public function edit(
        SellProposal $sellProposal,
        Request $request,
        TranslatorInterface $translator,
        UserNotificationRepository $userNotificationRepository
    ) {
        $currentUser = $this->getUser();
        $seller = $sellProposal->getSeller()->getUser();
        if (in_array('ROLE_ADMIN', $currentUser->getRoles()) or $currentUser == $seller) {
            $countNotifications = $userNotificationRepository->unreadCount($currentUser);
            $firstNotifications = $userNotificationRepository->findBy(['user' => $currentUser], ['id' => 'DESC'], '5');

            $form = $this->createForm(
                SellProposalType::class,
                $sellProposal,
                [
                    'current_user' => $currentUser
                ]
            );
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->em->persist($sellProposal);
                $this->em->flush();
                $this->addFlash(
                    'success',
                    $translator->trans(
                        'The proposal has been edited, the merchant who launched the search has been notified.'
                    )
                );

                $notification = new Notification(__FUNCTION__, $sellProposal);
                $this->em->persist($notification);
                $this->em->flush();
                $this->em->refresh($notification);

                $userNotification = new UserNotification();
                $userNotification->setUser($sellProposal->getProductSearch()->getMerchant()->getUser());
                $userNotification->setNotification($notification);
                $this->em->persist($userNotification);
                $this->em->flush();

                return $this->redirectToRoute('my_sell_proposal_list');
            }

            return $this->render(
                'sellProposal/edit.html.twig',
                [
                    'current_menu'        => 'sell_proposal_list',
                    'sellProposal'        => $sellProposal,
                    'productSearched'     => $sellProposal->getProductSearch(),
                    'form'                => $form->createView(),
                    'first_notifications' => $firstNotifications,
                    'count_notifications' => $countNotifications
                ]
            );
        } else {
            $this->addFlash(
                'danger',
                $translator->trans('You cannot edit this sell proposal')
            );

            return $this->redirectToRoute('my_sell_proposal_list');
        }
    }

    /**
     * @Route("/admin/liste-propositions", name="admin_sell_proposal_list")
     * @param  SellProposalRepository     $repository
     * @param  UserNotificationRepository $userNotificationRepository
     * @param  PaginatorInterface         $paginator
     * @param  Request                    $request
     * @return Response
     */
    public function list(
        SellProposalRepository $repository,
        UserNotificationRepository $userNotificationRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $allSellProposals = $repository->findBy([], ['id' => 'DESC']);
        $proposals = $paginator->paginate(
            $allSellProposals,
            $request->query->getInt('page', 1),
            10
        );
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(
            ['user' => $currentUser],
            ['id' => 'DESC'],
            '5'
        );

        return $this->render(
            'sellProposal/sellProposals.html.twig',
            [
                'current_menu'        => 'admin_sell_proposal_list',
                'sellProposals'       => $allSellProposals,
                'proposals'           => $proposals,
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/marchand/mes-propositions", name="my_sell_proposal_list")
     * @param  SellProposalRepository     $repository
     * @param  UserNotificationRepository $userNotificationRepository
     * @param  MerchantRepository         $merchantRepository
     * @param  PaginatorInterface         $paginator
     * @param  Request                    $request
     * @return Response
     */
    public function myProposalList(
        SellProposalRepository $repository,
        UserNotificationRepository $userNotificationRepository,
        MerchantRepository $merchantRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $currentUser = $this->getUser();
        $currentMerchant = $merchantRepository->findOneBy(['user' => $currentUser->getId()]);

        $allMyProposals = $repository->findBy(
            ['seller' => $currentMerchant],
            ['id' => 'DESC']
        );

        $proposals = $paginator->paginate(
            $allMyProposals,
            $request->query->getInt('page', 1),
            10
        );

        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(['user' => $currentUser], ['id' => 'DESC'], '5');

        return $this->render(
            'sellProposal/myProposals.html.twig',
            [
                'current_menu'        => 'my_sell_proposal_list',
                'current_user'        => $currentUser,
                'proposals'           => $proposals,
                'myProposals'         => $allMyProposals,
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/marchand/liste-propositions/recherche-{search}", name="sell_proposal_list")
     * @param  SellProposalRepository     $repository
     * @param  ProductSearch              $search
     * @param  UserNotificationRepository $userNotificationRepository
     * @param  PaginatorInterface         $paginator
     * @param  Request                    $request
     * @return Response
     */
    public function listFromSearch(
        SellProposalRepository $repository,
        ProductSearch $search,
        UserNotificationRepository $userNotificationRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(
            ['user' => $currentUser],
            ['id' => 'DESC'],
            '5'
        );
        $allSellProposalsSearch = $repository->findBy(
            ['productSearch' => $search],
            ['unitPrice' => 'DESC']
        );

        $proposals = $paginator->paginate(
            $allSellProposalsSearch,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render(
            'sellProposal/sellProposalsToSearch.html.twig',
            [
                'current_menu'        => 'sell_proposal_list',
                'sellProposals'       => $allSellProposalsSearch,
                'proposals'           => $proposals,
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/marchand/desactiver-proposition/{sellProposal}", name="disable_proposal")
     * @param  SellProposal        $sellProposal
     * @param  TranslatorInterface $translator
     * @return RedirectResponse
     */
    public function disable(SellProposal $sellProposal, TranslatorInterface $translator): RedirectResponse
    {
        $currentUser = $this->getUser();
        $seller = $sellProposal->getSeller()->getUser();
        if (in_array('ROLE_ADMIN', $currentUser->getRoles()) or $currentUser == $seller) {
            $sellProposal->setPublished("0");

            $this->em->persist($sellProposal);
            $this->em->flush();
            $this->addFlash(
                'success',
                $translator->trans('Sell proposal has been successfully disabled')
            );
        } else {
            $this->addFlash(
                'warning',
                $translator->trans('Sorry, you can not do this action')
            );
        }
        return $this->redirectToRoute('my_sell_proposal_list');
    }

    /**
     * @Route("/marchand/activer-proposition/{sellProposal}", name="enable_proposal")
     * @param  SellProposal        $sellProposal
     * @param  TranslatorInterface $translator
     * @return RedirectResponse
     */
    public function enable(SellProposal $sellProposal, TranslatorInterface $translator): RedirectResponse
    {
        $currentUser = $this->getUser();
        $seller = $sellProposal->getSeller()->getUser();
        if (in_array('ROLE_ADMIN', $currentUser->getRoles()) or $currentUser == $seller) {
            $sellProposal->setPublished("1");

            $this->em->persist($sellProposal);
            $this->em->flush();
            $this->addFlash(
                'success',
                $translator->trans('Sell proposal has been successfully enabled')
            );
        } else {
            $this->addFlash(
                'warning',
                $translator->trans('Sorry, you can not do this action')
            );
        }
        return $this->redirectToRoute('my_sell_proposal_list');
    }

    /**
     * @Route("/marchand/proposition-{sellProposal}", name="show_proposal")
     * @param  SellProposalRepository      $repository
     * @param  SellProposal                $sellProposal
     * @param  UserNotificationRepository  $userNotificationRepository
     * @param  TransportProposalRepository $transportProposalRepository
     * @param  TransportSearchRepository   $transportSearchRepository
     * @return Response
     */
    public function show(
        SellProposalRepository $repository,
        SellProposal $sellProposal,
        UserNotificationRepository $userNotificationRepository,
        TransportProposalRepository $transportProposalRepository,
        TransportSearchRepository $transportSearchRepository
    ): Response {
        $oneSellProposal = $repository->findOneBy(['id' => $sellProposal]);
        $searchTransporterSend = $transportSearchRepository->findOneBy(['sellProposal' => $sellProposal]);
        $transportProposals = [];
        $transportAccepted = [];

        if ($searchTransporterSend) {
            $transportProposals = $transportProposalRepository->findBy(
                ['sellProposal' => $sellProposal],
                ['deliveryTime' => 'ASC']
            );
            $transportAccepted = $transportProposalRepository->findOneBy(
                ['sellProposal' => $sellProposal, 'buyerAgreement' => 1],
                ['deliveryTime' => 'ASC']
            );
        }
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(
            ['user' => $currentUser],
            ['id' => 'DESC'],
            '5'
        );

        return $this->render(
            'sellProposal/sellProposal.html.twig',
            [
                'current_menu'            => 'show_proposal',
                'current_user'            => $currentUser,
                'sellProposal'            => $oneSellProposal,
                'transport_proposals'     => $transportProposals,
                'transport_accepted'      => $transportAccepted,
                'search_transporter_send' => $searchTransporterSend,
                'first_notifications'     => $firstNotifications,
                'count_notifications'     => $countNotifications
            ]
        );
    }
}

<?php

namespace App\Controller;

use App\Entity\ProductCategory;
use App\Form\ProductCategoryType;
use App\Repository\ProductCategoryRepository;
use App\Repository\UserNotificationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProductCategoryController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/marchand/categories", name="categories_list")
     * @param  ProductCategoryRepository  $repository
     * @param  UserNotificationRepository $userNotificationRepository
     * @return Response
     */
    public function list(
        ProductCategoryRepository $repository,
        UserNotificationRepository $userNotificationRepository
    ): Response {
        $allCategories = $repository->findAll();
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(
            ['user' => $currentUser],
            ['id' => 'DESC'],
            '5'
        );

        return $this->render(
            'productCategory/productCategories.html.twig',
            [
                'current_menu'        => 'categories_list',
                'categories'          => $allCategories,
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/marchand/nouvelle-categorie", name="category_add")
     * @param  Request                    $request
     * @param  TranslatorInterface        $translator
     * @param  UserNotificationRepository $userNotificationRepository
     * @return RedirectResponse|Response
     */
    public function new(
        Request $request,
        TranslatorInterface $translator,
        UserNotificationRepository $userNotificationRepository
    ) {
        $category = new ProductCategory();
        $category->setPublished('1');
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(
            ['user' => $currentUser],
            ['id' => 'DESC'],
            '5'
        );

        $form = $this->createForm(
            ProductCategoryType::class,
            $category,
            [
                'current_user' => $currentUser
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($category);
            $this->em->flush();
            $this->addFlash(
                'success',
                $translator->trans('The category has been added.')
            );
            return $this->redirectToRoute('categories_list');
        }

        return $this->render(
            'productCategory/new.html.twig',
            [
                'current_menu'        => 'category_add',
                'category'            => $category,
                'current_user'        => $currentUser,
                'form'                => $form->createView(),
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/marchand/edit-categorie/{category}", name="category_edit")
     * @Security("is_granted('ROLE_ADMIN')")
     * @param  ProductCategory            $category
     * @param  Request                    $request
     * @param  TranslatorInterface        $translator
     * @param  UserNotificationRepository $userNotificationRepository
     * @return RedirectResponse|Response
     */
    public function edit(
        ProductCategory $category,
        Request $request,
        TranslatorInterface $translator,
        UserNotificationRepository $userNotificationRepository
    ) {
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(
            ['user' => $currentUser],
            ['id' => 'DESC'],
            '5'
        );

        $form = $this->createForm(
            ProductCategoryType::class,
            $category,
            [
                'current_user' => $currentUser
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash(
                'success',
                $translator->trans('The category has been edited.')
            );

            return $this->redirectToRoute(
                'categories_list',
                [
                    'categoryId' => $category->getId()
                ]
            );
        }

        return $this->render(
            'productCategory/edit.html.twig',
            [
                'current_menu'        => 'category_edit',
                'category'            => $category,
                'current_user'        => $currentUser,
                'form'                => $form->createView(),
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/admin/categorie/edit/{id}", name="admin_category_delete", methods="DELETE")
     * @param                               ProductCategory     $category
     * @param                               Request             $request
     * @param                               TranslatorInterface $translator
     * @return                              Response
     */
    public function delete(ProductCategory $category, Request $request, TranslatorInterface $translator): Response
    {
        if ($this->isCsrfTokenValid('deleteCategory' . $category->getId(), $request->get('_token'))) {
            $this->em->remove($category);
            $this->addFlash(
                'warning',
                $translator->trans('The category has been deleted.')
            );
            $this->em->flush();
        }
        return $this->redirectToRoute('categories_list');
    }
}

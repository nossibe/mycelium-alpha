<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserNotificationRepository;

class HomeController extends AbstractController
{

    /**
     * @param      UserNotificationRepository $userNotificationRepository
     * @return     Response
     * @Route("/", name="home")
     */
    public function index(UserNotificationRepository $userNotificationRepository): Response
    {
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(
            ['user' => $currentUser],
            ['id' => 'DESC'],
            '5'
        );
        return $this->render(
            'pages/home.html.twig',
            [
                'current_menu' => 'home',
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }
}

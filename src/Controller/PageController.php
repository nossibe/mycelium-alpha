<?php

namespace App\Controller;

use App\Repository\UserNotificationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractController
{

    /**
     * @Route("/membre/choix-profil", name="profile-choice")
     * @param  UserNotificationRepository $userNotificationRepository
     * @return RedirectResponse|Response
     */
    public function index(UserNotificationRepository $userNotificationRepository)
    {
        $currentUser = $this->getUser();
        $userRoles = $currentUser->getRoles();
        if (in_array('ROLE_MERCHANT', $currentUser->getRoles()) or in_array('ROLE_TRANSPORTER', $userRoles)) {
            return $this->redirectToRoute('home');
        } else {
            $countNotifications = $userNotificationRepository->unreadCount($currentUser);
            $firstNotifications = $userNotificationRepository->findBy(['user' => $currentUser], ['id' => 'DESC'], '5');

            return $this->render(
                'pages/profile-choice.html.twig',
                [
                    'current_menu' => 'home',
                    'first_notifications' => $firstNotifications,
                    'count_notifications' => $countNotifications
                ]
            );
        }
    }
}

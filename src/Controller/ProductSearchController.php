<?php

namespace App\Controller;

use App\Entity\Merchant;
use App\Entity\ProductSearch;
use App\Entity\Notification;
use App\Entity\UserNotification;
use App\Form\ProductSearchType;
use App\Repository\MerchantRepository;
use App\Repository\ProductSearchRepository;
use App\Repository\SellProposalRepository;
use App\Repository\TransportProposalRepository;
use App\Repository\TransportSearchRepository;
use App\Repository\UserNotificationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProductSearchController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/marchand/nouvelle-recherche", name="product_search_add")
     * @param  Request                    $request
     * @param  TranslatorInterface        $translator
     * @param  UserNotificationRepository $userNotificationRepository
     * @return RedirectResponse|Response
     */
    public function new(
        Request $request,
        TranslatorInterface $translator,
        UserNotificationRepository $userNotificationRepository
    ) {
        $search = new ProductSearch();

        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(['user' => $currentUser], ['id' => 'DESC'], '5');

        $form = $this->createForm(
            ProductSearchType::class,
            $search,
            [
                'current_user' => $currentUser
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($search);
            $this->em->persist($search->getProduct());
            $this->em->flush();
            $this->addFlash(
                'success',
                $translator->trans('The search has been launched.')
            );
            $this->em->refresh($search); // to get $search id after flush
            $notification = new Notification(__FUNCTION__, $search);
            $this->em->persist($notification);
            $this->em->flush();
            $this->em->refresh($notification); // to get $notification id after flush

            $productSearchedCategory = $search->getProduct()->getCategory();
            $categoryFollowers = $productSearchedCategory->getFollowers();

            foreach ($categoryFollowers as $merchantFollower) {
                if ($merchantFollower->getUser() !== $currentUser) {
                    $userNotification = new UserNotification();
                    $userNotification->setUser($merchantFollower->getUser());
                    $userNotification->setNotification($notification);
                    $this->em->persist($userNotification);
                }
            }
            $this->em->flush();

            return $this->redirectToRoute('my_product_searches_list');
        }

        return $this->render(
            'productSearch/new.html.twig',
            [
                'current_menu'        => 'product_searches_list',
                'productSearch'       => $search,
                'current_user'        => $currentUser,
                'form'                => $form->createView(),
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }

    /**
     * @Route("/marchand/modifier-recherche/{productSearch}", name="product_search_edit")
     * @param  ProductSearch              $productSearch
     * @param  Request                    $request
     * @param  TranslatorInterface        $translator
     * @param  UserNotificationRepository $userNotificationRepository
     * @return RedirectResponse|Response
     */
    public function edit(
        ProductSearch $productSearch,
        Request $request,
        TranslatorInterface $translator,
        UserNotificationRepository $userNotificationRepository
    ) {
        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(['user' => $currentUser], ['id' => 'DESC'], '5');
        $deadLine = $productSearch
            ->getDeadLine()
            ->format('d-m-Y H:m'); //On edit form, object DateTime don not work, we use string

        $form = $this->createForm(
            ProductSearchType::class,
            $productSearch,
            [
                'current_user' => $currentUser,
                'dead_line' => $deadLine
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($productSearch);
            $this->em->persist($productSearch->getProduct());
            $this->em->flush();
            $this->addFlash(
                'success',
                $translator->trans('The search has been edited.')
            );

            $notification = new Notification(__FUNCTION__, $productSearch);
            $this->em->persist($notification);
            $this->em->flush();
            $this->em->refresh($notification); // to get $notification id after flush

            $productSearchedCategory = $productSearch->getProduct()->getCategory();
            $categoryFollowers = $productSearchedCategory->getFollowers();

            foreach ($categoryFollowers as $merchantFollower) {
                if ($merchantFollower->getUser() !== $currentUser) {
                    $userNotification = new UserNotification();
                    $userNotification->setUser($merchantFollower->getUser());
                    $userNotification->setNotification($notification);
                    $this->em->persist($userNotification);
                }
            }
            $this->em->flush();

            return $this->redirectToRoute('product_search_edit', ['productSearch' => $productSearch->getId()]);
        }

        return $this->render(
            'productSearch/edit.html.twig',
            [
                'current_menu'        => 'my_product_searches_list',
                'productSearch'       => $productSearch,
                'current_user'        => $currentUser,
                'form'                => $form->createView(),
                'first_notifications' => $firstNotifications,
                'count_notifications' => $countNotifications
            ]
        );
    }


    /**
     * @Route("/recherche/{productSearchId}", name="product_search_page")
     * @param  ProductSearchRepository    $repository
     * @param  string                     $productSearchId
     * @param  UserNotificationRepository $userNotificationRepository
     * @param  SellProposalRepository     $sellProposalRepository
     * @param  MerchantRepository         $merchantRepository
     * @return Response
     */
    public function page(
        ProductSearchRepository $repository,
        string $productSearchId,
        UserNotificationRepository $userNotificationRepository,
        SellProposalRepository $sellProposalRepository,
        MerchantRepository $merchantRepository
    ): Response {
        $oneProductSearch = $repository->findOneBy(['id' => $productSearchId]);

        $currentUser = $this->getUser();
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(['user' => $currentUser], ['id' => 'DESC'], '5');

        $currentSeller = $merchantRepository->findOneBy(['user' => $currentUser]);
        $currentUserProposalSearches = $sellProposalRepository
            ->findUserProposalsForThisSearch($currentSeller, $oneProductSearch);

        return $this->render(
            'productSearch/oneProductSearch.html.twig',
            [
                'current_menu'                   => 'product_searches_list',
                'current_user'                   => $currentUser,
                'product_search'                 => $oneProductSearch,
                'current_user_proposal_searches' => $currentUserProposalSearches,
                'first_notifications'            => $firstNotifications,
                'count_notifications'            => $countNotifications
            ]
        );
    }

    /**
     * @Route("/marchand/recherches-produits", name="product_searches_list")
     * @param  ProductSearchRepository    $repository
     * @param  UserNotificationRepository $userNotificationRepository
     * @param  SellProposalRepository     $sellProposalRepository
     * @param  MerchantRepository         $merchantRepository
     * @param  PaginatorInterface         $paginator
     * @param  Request                    $request
     * @return Response
     */
    public function list(
        ProductSearchRepository $repository,
        UserNotificationRepository $userNotificationRepository,
        SellProposalRepository $sellProposalRepository,
        MerchantRepository $merchantRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $currentUser = $this->getUser();

        $allSearches = $repository->findAllButMine($currentUser);

        $searches = $paginator->paginate(
            $allSearches,
            $request->query->getInt('page', 1),
            12
        );

        $currentSeller = $merchantRepository->findOneBy(['user' => $currentUser]);
        $currentUserProposals = $sellProposalRepository->findBy(['seller' => $currentSeller]);
        $currentUserProposalSearches = $sellProposalRepository->findProductSearches($currentSeller);

        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(['user' => $currentUser], ['id' => 'DESC'], '5');

        return $this->render(
            'productSearch/productSearches.html.twig',
            [
                'current_menu'                   => 'product_searches_list',
                'current_user'                   => $currentUser,
                'current_user_proposals'         => $currentUserProposals,
                'current_user_proposal_searches' => $currentUserProposalSearches,
                'searches'                       => $searches,
                'first_notifications'            => $firstNotifications,
                'count_notifications'            => $countNotifications
            ]
        );
    }

    /**
     * @Route("/marchand/mes-produits", name="my_product_searches_list")
     * @param  ProductSearchRepository    $repository
     * @param  SellProposalRepository     $sellProposalRepository
     * @param  UserNotificationRepository $userNotificationRepository
     * @param  TransportSearchRepository  $transportSearchRepository
     * @param  PaginatorInterface         $paginator
     * @param  Request                    $request
     * @return Response
     */
    public function myResearchList(
        ProductSearchRepository $repository,
        SellProposalRepository $sellProposalRepository,
        UserNotificationRepository $userNotificationRepository,
        TransportSearchRepository $transportSearchRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $currentUser = $this->getUser();

        $currentUserMerchant = $this->em->getRepository(Merchant::class)->findBy(array('user' => $currentUser));
        $countNotifications = $userNotificationRepository->unreadCount($currentUser);
        $firstNotifications = $userNotificationRepository->findBy(['user' => $currentUser], ['id' => 'DESC'], '5');

        $sellProposalTransporterSearchAlreadySent = $transportSearchRepository
            ->findSellProposalTransporterSearchAlreadySent();
        $sellProposalOfTransportSearchesClosed = $transportSearchRepository->findTransportSearchWithBuyerAgreement();
        $allMyResearch = $repository->findBy(
            ['merchant' => $currentUserMerchant],
            ['id' => 'DESC']
        );

        $searchSellProposals = $sellProposalRepository->searchesSellProposals($allMyResearch);

        $searches = $paginator->paginate(
            $allMyResearch,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render(
            'productSearch/myProductSearches.html.twig',
            [
                'current_menu'                    => 'my_product_searches_list',
                'searches'                        => $searches,
                'current_user'                    => $currentUser,
                'search_sell_proposals'           => $searchSellProposals,
                'first_notifications'             => $firstNotifications,
                'count_notifications'             => $countNotifications,
                'sell_proposal_search_transporter_already_sent' => $sellProposalTransporterSearchAlreadySent,
                'sell_proposal_of_transport_searches_closed' => $sellProposalOfTransportSearchesClosed
            ]
        );
    }

    /**
     * @Route("/marchand/desactiver-recherche/{search}", name="disable_search")
     * @param  ProductSearch               $search
     * @param  TransportProposalRepository $transportProposalRepository
     * @param  TranslatorInterface         $translator
     * @return RedirectResponse
     */
    public function disable(
        ProductSearch $search,
        TransportProposalRepository $transportProposalRepository,
        TranslatorInterface $translator
    ): RedirectResponse {
        $currentUser = $this->getUser();

        if (in_array('ROLE_ADMIN', $currentUser->getRoles()) or $currentUser == $search->getMerchant()->getUser()) {
            $transportProposalAgree = $transportProposalRepository->findTransportProposalWithAgreement($search);
            if (!$transportProposalAgree) {
                $search->setPublished("0");

                $this->em->persist($search);
                $this->em->flush();
                $this->addFlash(
                    'success',
                    $translator->trans('Search has been successfully disabled')
                );
            } else {
                $this->addFlash(
                    'warning',
                    $translator->trans('Sorry, you can not do this action. An offer has already been accepted')
                );
            }
        } else {
            $this->addFlash(
                'warning',
                $translator->trans('Sorry, you can not do this action')
            );
        }
        return $this->redirectToRoute('my_product_searches_list');
    }

    /**
     * @Route("/marchand/activer-recherche/{search}", name="enable_search")
     * @param  ProductSearch       $search
     * @param  TranslatorInterface $translator
     * @return RedirectResponse
     */
    public function enable(ProductSearch $search, TranslatorInterface $translator): RedirectResponse
    {
        $currentUser = $this->getUser();

        if (in_array('ROLE_ADMIN', $currentUser->getRoles()) or $currentUser == $search->getMerchant()->getUser()) {
            $search->setPublished("1");

            $this->em->persist($search);
            $this->em->flush();
            $this->addFlash(
                'success',
                $translator->trans('Search has been successfully enabled')
            );
        } else {
            $this->addFlash(
                'warning',
                $translator->trans('Sorry, you can not do this action')
            );
        }
        return $this->redirectToRoute('my_product_searches_list');
    }


    /**
     * @Route("/admin/supprimer-recherche/{id}", name="admin_product_search_delete", methods="DELETE")
     * @param  ProductSearch       $search
     * @param  Request             $request
     * @param  TranslatorInterface $translator
     * @return Response
     */
    public function delete(ProductSearch $search, Request $request, TranslatorInterface $translator): Response
    {
        if ($this->isCsrfTokenValid('deleteMerchant' . $search->getId(), $request->get('_token'))) {
            $this->em->remove($search);
            $this->addFlash(
                'warning',
                $translator->trans('The search has been deleted.')
            );
            $this->em->flush();
        }
        return $this->redirectToRoute('product_searches_list');
    }
}

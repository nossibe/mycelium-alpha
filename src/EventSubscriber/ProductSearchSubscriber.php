<?php

namespace App\EventSubscriber;

use App\Entity\Merchant;
use App\Entity\ProductSearch;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ProductSearchSubscriber implements EventSubscriber
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $productSearch = $args->getObject();

        if ($productSearch instanceof ProductSearch) {
            if (null === $productSearch->getMerchant()) {
                $this->addMerchant($productSearch);
            }
        }
        return;
    }

    public function addMerchant(ProductSearch $productSearch)
    {
        $currentUser = $this->tokenStorage->getToken()->getUser();
        $currentUserMerchant = $currentUser->getMerchant();

        if ($currentUserMerchant instanceof Merchant) {
            $productSearch->setMerchant($currentUserMerchant);
        }
    }
}

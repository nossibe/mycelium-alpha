<?php

namespace App\EventSubscriber;

use App\Entity\ProductSearch;
use App\Entity\SellProposal;
use App\Entity\TransportProposal;
use App\Entity\TransportSearch;
use App\Entity\UserNotification;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UserNotificationSubscriber implements EventSubscriber
{

    private $mailer;
    private $router;
    private $translator;
    private $em;

    public function __construct(
        EntityManagerInterface $em,
        MailerInterface $mailer,
        UrlGeneratorInterface $router,
        TranslatorInterface $translator
    ) {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->translator = $translator;
        $this->em = $em;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist
        ];
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $userNotification = $args->getObject();

        if ($userNotification instanceof UserNotification) {
            try {
                $this->sendEmailNotification($userNotification, $args);
            } catch (TransportExceptionInterface $e) {
                $errorMessage = $e->getMessage();
                $response = new Response();
                $response -> setContent($errorMessage);
                $response -> setStatusCode(Response::HTTP_BAD_REQUEST);
            }
            return;
        }
    }

    /**
     * @param  UserNotification   $userNotification
     * @throws TransportExceptionInterface
     */
    public function sendEmailNotification(UserNotification $userNotification)
    {
        $context = null;
        $subject = $this->translator->trans("New notification");
        $objectChanged = $userNotification->getNotification()->getObjectChanged();
        $eventType = $userNotification->getNotification()->getEventType();
        $textTemplate = "emails/" . $objectChanged . "/" . $eventType . ".txt.twig";
        $htmlTemplate = "emails/" . $objectChanged . "/" . $eventType . ".html.twig";

        if ($objectChanged == "ProductSearch") {
            $productSearch = $this->em->getRepository(ProductSearch::class)->find(
                $userNotification->getNotification()->getObjectId()
            );
            $context = [
                'context_category' => $productSearch
                    ->getProduct()
                    ->getCategory()
                    ->getName(),
                'context_link' => $this->router->generate(
                    'product_search_page',
                    [
                        'productSearchId' => $productSearch->getId()
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                )
            ];
            if ($eventType === "new") {
                $subject = $this->translator->trans("New product search");
            } else {
                $subject = $this->translator->trans("Product search edited");
            }
        } elseif ($objectChanged == "SellProposal") {
            $sellProposal = $this->em->getRepository(SellProposal::class)->find(
                $userNotification->getNotification()->getObjectId()
            );
            $context = [
                'context_product_searched' => $sellProposal
                    ->getProductSearch()
                    ->getProduct()
                    ->getName(),
                'context_link' => $this->router->generate(
                    'show_proposal',
                    [
                        'sellProposal' => $sellProposal->getId()
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                )
            ];
            if ($eventType === "new") {
                $subject = $this->translator->trans("New sell proposal");
            } else {
                $subject = $this->translator->trans("Sell proposal edited");
            }
        } elseif ($objectChanged == "TransportSearch") {
            $transportSearch = $this->em->getRepository(TransportSearch::class)->find(
                $userNotification->getNotification()->getObjectId()
            );
            $sellProposal = $transportSearch->getSellProposal();
            $context = [
                'context_sell_proposal' => $sellProposal
                    ->getProductSearch()
                    ->getProduct()
                    ->getName(),
                'context_link' => $this->router->generate(
                    'transporters_search_list',
                    [],
                    UrlGeneratorInterface::ABSOLUTE_URL
                )
            ];
            if ($eventType === "new") {
                $subject = $this->translator->trans("New transport search");
            } else {
                $subject = $this->translator->trans("Transport Search edited");
            }
        } elseif ($objectChanged == "TransportProposal") {
            $transportProposal = $this->em->getRepository(TransportProposal::class)->find(
                $userNotification->getNotification()->getObjectId()
            );
            $sellProposal = $transportProposal->getSellProposal();
            $context = [
                'context_product_searched' => $sellProposal
                    ->getproductSearch()
                    ->getProduct()
                    ->getName(),
                'context_link' => $this->router->generate(
                    'show_proposal',
                    [
                        'sellProposal' => $sellProposal->getId()
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                )
            ];
            if ($eventType === "new") {
                $subject = $this->translator->trans("New transport proposal");
            } elseif ($eventType === "buyerAgree") {
                $transporter = $transportProposal->getTransporter()->getUser();
                if ($userNotification->getUser() == $sellProposal->getSeller()->getUser()) {
                    $context['context_transporter_name'] = $transportProposal
                        ->getTransporter()
                        ->getUser()
                        ->getFullName();
                    $context['context_recovery_date'] = $transportProposal->getRecoveryTime();
                    $textTemplate = "emails/" . $objectChanged . "/" . $eventType . "Seller.txt.twig";
                    $htmlTemplate = "emails/" . $objectChanged . "/" . $eventType . "Seller.html.twig";
                } elseif ($userNotification->getUser() == $transporter) {
                    $context['context_seller'] = $sellProposal
                        ->getSeller()
                        ->getBusinessName();
                    $context['context_seller_address'] = $sellProposal
                        ->getSeller()
                        ->getAddress();
                    $context['context_seller_address'] .= ' ';
                    $context['context_seller_address'] .= $sellProposal
                            ->getSeller()
                            ->getCity()
                            ->getPostalCode();
                    $context['context_seller_address'] .= ' ';
                    $context['context_seller_address'] .= $sellProposal
                        ->getSeller()
                        ->getCity()
                        ->getName();
                    $context['context_buyer'] = $sellProposal
                        ->getProductSearch()
                        ->getMerchant()
                        ->getBusinessName();
                    $context['context_buyer_address'] = $sellProposal
                            ->getProductSearch()
                            ->getMerchant()
                            ->getAddress();
                    $context['context_buyer_address'] .= ' ';
                    $context['context_buyer_address'] .= $sellProposal
                            ->getProductSearch()
                            ->getMerchant()
                            ->getCity()
                            ->getPostalCode();
                    $context['context_buyer_address'] .= ' ';
                    $context['context_buyer_address'] .= $sellProposal
                        ->getProductSearch()
                        ->getMerchant()
                        ->getCity()
                        ->getName();
                    $textTemplate = "emails/" . $objectChanged . "/" . $eventType . "Transporter.txt.twig";
                    $htmlTemplate = "emails/" . $objectChanged . "/" . $eventType . "Transporter.html.twig";
                }
                $subject = $this->translator->trans("Your proposal has been accepted");
            } elseif ($eventType === "delete") {
                $subject = $this->translator->trans("Transport proposal deleted");
            } elseif ($eventType === "isInDelivering") {
                $context = [
                    'context_product_searched' => $sellProposal
                        ->getProductSearch()
                        ->getProduct()
                        ->getName(),
                    'context_link' => $this->router->generate(
                        'show_proposal',
                        [
                            'sellProposal' => $sellProposal->getId()
                        ],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    )
                ];
                $subject = $this->translator->trans("Transport in progress");
            } elseif ($eventType === "isDelivered") {
                $context = [
                    'context_product_searched' => $sellProposal
                        ->getProductSearch()
                        ->getProduct()
                        ->getName(),
                    'context_link' => $this->router->generate(
                        'show_proposal',
                        [
                            'sellProposal' => $sellProposal->getId()
                        ],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    )
                ];
                $subject = $this->translator->trans("Delivery made");
            } else {
                $subject = $this->translator->trans("Transport proposal edited");
            }
        }

        $email = (new TemplatedEmail())
            ->from('no-reply@mycelium.cayoo.fr')
            ->to($userNotification->getUser()->getEmail())
            ->subject($subject)
            ->textTemplate($textTemplate)
            ->htmlTemplate($htmlTemplate);

        $email->getHeaders()->addTextHeader('X-Auto-Response-Suppress', 'OOF, DR, RN, NRN, AutoReply');

        if ($context) {
            $email->context($context);
        }
        $this->mailer->send($email);
    }
}

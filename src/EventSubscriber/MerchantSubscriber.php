<?php

namespace App\EventSubscriber;

use App\Entity\Merchant;
use App\Entity\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MerchantSubscriber implements EventSubscriber
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::onFlush,
            Events::prePersist,
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $em = $args->getObjectManager();
        $merchant = $args->getObject();

        if ($merchant instanceof Merchant) {
            if (null === $merchant->getUser()) {
                $this->addUser($merchant);
            }
        }
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        // Case Merchant deletion
        foreach ($uow->getScheduledEntityDeletions() as $entity) {
            if ($entity instanceof Merchant) {
                if ($entity->getUser() instanceof User) {
                    $this->deleteMerchantRole($entity->getUser());

                    $uow->computeChangeSets();
                    return;
                }
            }
        }

        // Case Merchant adding
        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if ($entity instanceof Merchant) {
                if ($entity->getUser() instanceof User) {
                    $this->addMerchantRole($entity->getUser());

                    $uow->computeChangeSets();
                    return;
                }
            }
        }

        // Case Merchant updating
        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof Merchant) {
                $changeSet = $uow->getEntityChangeSet($entity);

                if (isset($changeSet['user']) and $changeSet['user'][0] instanceof User) {
                    $oldOwner = $changeSet['user'][0];
                    $newOwner = $changeSet['user'][1];

                    $this->deleteMerchantRole($oldOwner);
                    $this->addMerchantRole($newOwner);

                    $uow->computeChangeSets();
                    return;
                }
            }
        }


        return;
    }

    public function deleteMerchantRole(User $user)
    {
        $rolesList = $user->getRoles();

        if (in_array('ROLE_MERCHANT', $rolesList)) {
            unset($rolesList[array_search('ROLE_MERCHANT', $rolesList)]);
        }

        if (count($rolesList) == 0) {
            $rolesList[] = 'ROLE_USER';
        }

        $user->setRoles($rolesList);
    }

    public function addMerchantRole(User $user)
    {
        $rolesList = $user->getRoles();

        if (!in_array('ROLE_MERCHANT', $rolesList)) {
            array_push($rolesList, 'ROLE_MERCHANT');
        }

        $user->setRoles($rolesList);
    }

    public function addUser(Merchant $merchant)
    {
        $currentUser = $this->tokenStorage->getToken()->getUser();
        if ($currentUser instanceof User) {
            $merchant->setUser($currentUser);
        }
    }
}

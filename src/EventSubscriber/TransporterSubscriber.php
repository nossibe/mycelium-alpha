<?php

namespace App\EventSubscriber;

use App\Entity\Transporter;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class TransporterSubscriber implements EventSubscriber
{
    private $em;
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
            Events::postUpdate,
        ];
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->em = $args->getObjectManager();
        $transporter = $args->getObject();

        if ($transporter instanceof Transporter) {
            $this->updateRole($transporter);
        }
        return;
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->em = $args->getObjectManager();
        $transporter = $args->getObject();

        if ($transporter instanceof Transporter) {
            $this->updateRole($transporter);
        }
        return;
    }

    public function updateRole(Transporter $transporter)
    {

        if ($transporter->getUser()) {
            $user = $transporter->getUser();
        } else {
            $user = $this->tokenStorage->getToken()->getUser();
            if (!( $user instanceof UserInterface )) {
                return;
            }
        }
        $rolesList = $user->getRoles();

        if (count($rolesList) == 0) {
            $rolesList[] = 'ROLE_USER';
        }

        if ($transporter->getPublished() === true) {
            if (!in_array('ROLE_TRANSPORTER', $rolesList)) {
                array_push($rolesList, 'ROLE_TRANSPORTER');

                $user->setRoles($rolesList);

                $this->em->flush();
            }
        } else {
            if (in_array('ROLE_TRANSPORTER', $rolesList)) {
                unset($rolesList[array_search('ROLE_TRANSPORTER', $rolesList)]);

                if (count($rolesList) == 0) {
                    $rolesList[] = 'ROLE_USER';
                }

                $user->setRoles($rolesList);

                $this->em->flush();
            }
        }
    }
}

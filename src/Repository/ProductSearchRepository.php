<?php

namespace App\Repository;

use App\Entity\ProductSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductSearch|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductSearch|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductSearch[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductSearchRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductSearch::class);
    }

    public function findAll()
    {
        return $this->findBy(array(), array('id' => 'DESC'));
    }

    public function findAllButMine(?\App\Entity\User $currentUser)
    {
        return $this->createQueryBuilder('u')
            ->leftJoin(
                'App\Entity\Merchant',
                'm',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'u.merchant = m'
            )
            ->andWhere('m.user != :currentUser')
            ->setParameter('currentUser', $currentUser)
            ->orderBy('u.id', 'DESC')
            ->getQuery()
            ->execute();
    }
}

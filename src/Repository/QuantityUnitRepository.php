<?php

namespace App\Repository;

use App\Entity\QuantityUnit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method QuantityUnit|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuantityUnit|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuantityUnit[]    findAll()
 * @method QuantityUnit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuantityUnitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, QuantityUnit::class);
    }
}

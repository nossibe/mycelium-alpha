<?php

namespace App\Repository;

use App\Entity\TransportProposal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TransportProposal|null find($id, $lockMode = null, $lockVersion = null)
 * @method TransportProposal|null findOneBy(array $criteria, array $orderBy = null)
 * @method TransportProposal[]    findAll()
 * @method TransportProposal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransportProposalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TransportProposal::class);
    }

    public function findTransporterSearchesIAnswered($currentUser)
    {
        return $this->createQueryBuilder('tp')
            ->select('ts')
            ->andWhere('tp.transporter = t')
            ->leftJoin(
                'App\Entity\Transporter',
                't',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                't.user = :currentUser'
            )
            ->leftJoin(
                'App\Entity\TransportSearch',
                'ts',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'ts = tp.transportSearch'
            )
            ->setParameter('currentUser', $currentUser)
            ->getQuery()
            ->getResult();
    }

    public function findMyTransportProposals($currentUser)
    {
        return $this->createQueryBuilder('tp')
            ->andWhere('tp.transporter = t')
            ->leftJoin(
                'App\Entity\Transporter',
                't',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                't.user = :currentUser'
            )
            ->setParameter('currentUser', $currentUser)
            ->getQuery()
            ->getResult();
    }

    public function findTransportProposalWithAgreement(\App\Entity\ProductSearch $search)
    {
        return $this->createQueryBuilder('tp')
            ->andWhere('tp.buyerAgreement = :buyerAgreement')
            ->andWhere('tp.sellProposal = sp')
            ->leftJoin(
                'App\Entity\SellProposal',
                'sp',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'sp.productSearch = :search'
            )
            ->setParameter('buyerAgreement', '1')
            ->setParameter('search', $search)
            ->getQuery()
            ->getResult();
    }
}

<?php

namespace App\Repository;

use App\Entity\City;
use App\Entity\Transporter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Transporter|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transporter|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transporter[]    findAll()
 * @method Transporter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransporterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transporter::class);
    }

    public function findCitiesServedFollowers(?City $city)
    {
        return $this->createQueryBuilder('t')
            ->select('t')
            ->join('t.citiesServed', 'tc')
            ->where('tc.id = :city')
            ->setParameter('city', $city)
            ->getQuery()
            ->getResult();
    }

    public function findMyCitiesServed($currentUser): array
    {
        $arrayOfArray =  $this->createQueryBuilder('t')
            ->join('t.citiesServed', 'tc')
            ->select('tc.id')
            ->where('t.user = :user')
            ->setParameter('user', $currentUser)
            ->getQuery()
            ->getScalarResult();
        $myCitiesServed = [];
        foreach ($arrayOfArray as $arrayCity) {
            $myCitiesServed[] = $arrayCity['id'];
        }
        return $myCitiesServed;
    }
}

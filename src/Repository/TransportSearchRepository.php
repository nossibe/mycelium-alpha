<?php

namespace App\Repository;

use App\Entity\TransportSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TransportSearch|null find($id, $lockMode = null, $lockVersion = null)
 * @method TransportSearch|null findOneBy(array $criteria, array $orderBy = null)
 * @method TransportSearch[]    findAll()
 * @method TransportSearch[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransportSearchRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TransportSearch::class);
    }

    public function findSellProposalTransporterSearchAlreadySent()
    {
        return $this->createQueryBuilder('ts')
            ->select('sp')
            ->leftJoin(
                'App\Entity\SellProposal',
                'sp',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'sp.id = ts.sellProposal'
            )
            ->getQuery()
            ->getResult();
    }

    public function findTransportSearchWithBuyerAgreement()
    {
        return $this->createQueryBuilder('ts')
            ->select('s')
            ->andWhere('tp.buyerAgreement LIKE :withAgreement')
            ->leftJoin(
                'App\Entity\TransportProposal',
                'tp',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'tp.transportSearch = ts'
            )
            ->leftJoin(
                'App\Entity\sellProposal',
                's',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'tp.sellProposal = s'
            )
            ->setParameter('withAgreement', true)
            ->getQuery()
            ->getResult();
    }

    public function findTransportSearchesClosed()
    {
        return $this->createQueryBuilder('ts')
            ->andWhere('tp.buyerAgreement LIKE :withAgreement')
            ->leftJoin(
                'App\Entity\TransportProposal',
                'tp',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'tp.transportSearch = ts'
            )
            ->setParameter('withAgreement', true)
            ->getQuery()
            ->getResult();
    }
}

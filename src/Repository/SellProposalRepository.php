<?php

namespace App\Repository;

use App\Entity\Merchant;
use App\Entity\ProductSearch;
use App\Entity\SellProposal;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method               SellProposal|null find($id, $lockMode = null, $lockVersion = null)
 * @method               SellProposal|null findOneBy(array $criteria, array $orderBy = null)
 * @method               SellProposal[]    findAll()
 * @method               SellProposal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @ORM\MappedSuperclass
 */
class SellProposalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SellProposal::class);
    }

    public function searchesSellProposals($productSearches)
    {
        return $this->createQueryBuilder('u')
            ->select('u')
            ->andWhere('u.productSearch IN (:productSearches)')
            ->setParameter('productSearches', $productSearches)
            ->getQuery()
            ->execute();
    }

    public function findProductSearches(?\App\Entity\Merchant $currentSeller)
    {
        return $this->createQueryBuilder('u')
            ->select('ps')
            ->leftJoin(
                'App\Entity\ProductSearch',
                'ps',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'u.productSearch = ps'
            )
            ->andWhere('u.seller = :currentSeller')
            ->setParameter('currentSeller', $currentSeller)
            ->getQuery()
            ->execute();
    }

    public function findUserProposalsForThisSearch(Merchant $currentSeller, ProductSearch $productSearch)
    {
        return $this->createQueryBuilder('u')
            ->select('u')
            ->andWhere('u.seller = :seller')
            ->andWhere('u.productSearch = :search')
            ->setParameter('seller', $currentSeller)
            ->setParameter('search', $productSearch)
            ->getQuery()
            ->execute();
    }
}

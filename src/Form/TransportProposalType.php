<?php

namespace App\Form;

use App\Entity\TransportProposal;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TransportProposalType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'recoveryTime',
                DateTimeType::class,
                [
                    'attr'   => ['class' => 'dateTimePicker', 'autocomplete' => 'off'],
                    'widget' => 'single_text',
                    'html5'  => false,
                    'format' => 'dd-MM-yyyy H:mm',
                    'input'  => 'string'
                ]
            )
            ->add(
                'deliveryTime',
                DateTimeType::class,
                [
                    'attr'   => ['class' => 'dateTimePicker', 'autocomplete' => 'off'],
                    'widget' => 'single_text',
                    'html5'  => false,
                    'format' => 'dd-MM-yyyy H:mm',
                    'input'  => 'string'
                ]
            );

        if (in_array('ROLE_ADMIN', $options['current_user']->getRoles())) {
            $builder
                ->add('buyerAgreement')
                ->add(
                    'published',
                    null,
                    [
                        'data'       => true,
                        'label_attr' => ['class' => 'switch-custom']
                    ]
                );
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
            'data_class'   => TransportProposal::class,
            'current_user' => false,
            ]
        );
    }
}

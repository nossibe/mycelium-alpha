<?php

namespace App\Form;

use App\Entity\Merchant;
use App\Entity\ProductSearch;
use App\Entity\QuantityUnit;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class ProductSearchType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (in_array('ROLE_ADMIN', $options['current_user']->getRoles())) {
            $builder
                ->add(
                    'merchant',
                    EntityType::class,
                    [
                        'class'        => Merchant::class,
                        'choice_label' => 'businessName',
                        'label'        => 'Applicant',
                        'attr'         => ['class' => 'custom-select']
                    ]
                );
        }
        $builder
            ->add('product', ProductType::class)
            ->add(
                'quantity',
                null,
                [
                    'empty_data' => '1'
                ]
            )
            ->add('transporterGift')
            ->add(
                'quantityUnit',
                EntityType::class,
                [
                    'class'                     => QuantityUnit::class,
                    'choice_label'              => 'name',
                    'choice_translation_domain' => true,
                    'attr'                      => ['class' => 'custom-select']
                ]
            )
            ->add('clarifications')
            ->add(
                'published',
                null,
                [
                    'data'       => true,
                    'label_attr' => ['class' => 'switch-custom']
                ]
            );
        if (!$options['data']->getId()) {
            $builder
                ->add(
                    'deadLine',
                    DateTimeType::class,
                    [
                        'attr'   => [
                            'class' => 'dateTimePicker',
                            'autocomplete' => 'off'
                        ],
                        'widget' => 'single_text',
                        'html5'  => false,
                        'format' => 'dd-MM-yyyy H:mm',
                        'input'  => 'string'
                    ]
                );
        } else {
            // On edit form, object DateTime don not work, we use string
            $builder
                ->add('deadLine', TextType::class, ['data' => $options['dead_line']]);
        }

        $user = $options['current_user'];
        if (!$user) {
            throw new \LogicException(
                'The FriendMessageFormType cannot be used without an authenticated user!'
            );
        }

        if (!in_array('ROLE_ADMIN', $user->getRoles())) {
            $builder->addEventListener(
                FormEvents::PRE_SET_DATA,
                function (FormEvent $event) use ($user) {
                    if (null !== $event->getData()->getMerchant()) {
                        return;
                    }

                    $form = $event->getForm();
                    $search = $event->getData();

                    if (null === $search) {
                        return;
                    }


                    $formOptions = [
                    'class'        => Merchant::class,
                    'choice_label' => 'businessName',
                    'disabled'     => true,
                    'data'         => $user->getMerchant(),
                    'attr'         => [
                        'class' => 'custom-select'
                    ]
                    ];
                    $form->add('merchant', EntityType::class, $formOptions);
                }
            );
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
            'data_class'         => ProductSearch::class,
            'current_user'       => false,
            'dead_line'       => false,
            'translation_domain' => 'forms'
            ]
        );
    }
}

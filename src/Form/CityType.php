<?php

namespace App\Form;

use App\Entity\City;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class CityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                EntityType::class,
                [
                    'class'        => City::class,
                    'choice_label' => 'name',
                    'label'        => false,
                    'placeholder'  => 'Choose a city',
                    'required'     => false,
                    'attr'         => ['class' => 'custom-select']
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => City::class,
                'translation_domain' => 'forms'
            ]
        );
    }
}

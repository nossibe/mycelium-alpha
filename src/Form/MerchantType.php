<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Merchant;
use App\Entity\ProductCategory;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MerchantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('businessName')
            ->add(
                'published',
                CheckboxType::class,
                [
                    'label_attr' => ['class' => 'switch-custom'],
                    'required'   => false,
                    'data'       => true
                ]
            )
            ->add(
                'imageFile',
                FileType::class,
                [
                    'required'   => false
                ]
            );
        if (in_array('ROLE_ADMIN', $options['current_user']->getRoles())) {
            $builder
                ->add(
                    'user',
                    EntityType::class,
                    [
                        'class'        => User::class,
                        'label'        => 'transporterUserLabel',
                        'choice_label' => 'fullName',
                        'attr'         => ['class' => 'custom-select']
                    ]
                );
        }
        $builder
            ->add('address')
            ->add(
                'city',
                EntityType::class,
                [
                    'class'        => City::class,
                    'choice_label' => 'name',
                    'attr'         => ['class' => 'custom-select']
                ]
            )
            ->add(
                'categoriesFollowed',
                EntityType::class,
                [
                    'class'        => ProductCategory::class,
                    'label'        => 'CategoryFollowed',
                    'choice_label' => 'name',
                    'multiple'     => true,
                    'expanded'     => true
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'         => Merchant::class,
                'current_user'       => false,
                'translation_domain' => 'forms'
            ]
        );
    }
}

<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\ProductCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add(
                'ref',
                null,
                [
                    'required'   => false,
                ]
            )
            ->add(
                'category',
                EntityType::class,
                [
                    'class'        => ProductCategory::class,
                    'choice_label' => 'name',
                    'attr'         => ['class' => 'custom-select']
                ]
            )
            ->add(
                'imageFile',
                FileType::class,
                [
                    'required'   => false
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Product::class
            ]
        );
    }
}

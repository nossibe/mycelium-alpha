<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Transporter;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TransporterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'residenceCity',
                EntityType::class,
                [
                    'class'        => City::class,
                    'choice_label' => 'name',
                    'placeholder'  => 'Choose a city',
                    'attr'         => ['class' => 'custom-select']
                ]
            )
            ->add(
                'citiesServed',
                EntityType::class,
                [
                    'class'        => City::class,
                    'choice_label' => 'name',
                    'label'        => false,
                    'label_attr'   => ['class' => 'btn btn-outline-secondary'],
                    'expanded'     => true,
                    'multiple'     => true
                ]
            )
            ->add(
                'workCity',
                EntityType::class,
                [
                    'class'        => City::class,
                    'choice_label' => 'name',
                    'placeholder'  => 'Choose a city',
                    'attr'         => ['class' => 'custom-select']
                ]
            )
            ->add(
                'published',
                CheckboxType::class,
                [
                    'data'       => true,
                    'label_attr' => ['class' => 'switch-custom']
                ]
            );

        if (in_array('ROLE_ADMIN', $options['current_user']->getRoles())) {
            $builder
                ->add(
                    'user',
                    EntityType::class,
                    [
                        'class'        => User::class,
                        'choice_label' => 'fullName',
                        'attr'         => ['class' => 'custom-select']
                    ]
                );
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'         => Transporter::class,
                'current_user'       => false,
                'translation_domain' => 'forms'
            ]
        );
    }
}

<?php

namespace App\Form;

use App\Entity\Merchant;
use App\Entity\QuantityUnit;
use App\Entity\SellProposal;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SellProposalType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'published',
                CheckboxType::class,
                [
                    'data'       => true,
                    'label_attr' => ['class' => 'switch-custom']
                ]
            )
            ->add('unitPrice')
            ->add('qtyAvailable')
            ->add(
                'quantityUnit',
                EntityType::class,
                [
                    'class'                     => QuantityUnit::class,
                    'choice_translation_domain' => true,
                    'choice_label'              => 'name'
                ]
            );
        if (in_array('ROLE_ADMIN', $options['current_user']->getRoles())) {
            $builder
                ->add(
                    'seller',
                    EntityType::class,
                    [
                        'class'        => Merchant::class,
                        'choice_label' => 'businessName',
                        'attr'         => ['class' => 'custom-select']
                    ]
                );
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'         => SellProposal::class,
                'current_user'       => false,
                'translation_domain' => 'forms'
            ]
        );
    }
}

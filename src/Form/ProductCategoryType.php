<?php

namespace App\Form;

use App\Entity\ProductCategory;
use App\Repository\ProductCategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductCategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add(
                'published',
                CheckboxType::class,
                [
                    'label_attr' => ['class' => 'switch-custom']
                ]
            )
            ->add('followers');

        if (!$options['data']->getId()) {
            $builder
                ->add(
                    'parent',
                    EntityType::class,
                    [
                        'class'        => ProductCategory::class,
                        'choice_label' => 'name',
                        'placeholder'  => 'No parent category',
                        'required'     => false,
                        'attr'         => ['class' => 'custom-select']
                    ]
                );
        } else {
            $currentCatId = $options['data']->getId();
            $builder
                ->add(
                    'parent',
                    EntityType::class,
                    [
                        'class'         => ProductCategory::class,
                        'query_builder' => function (ProductCategoryRepository $repo) use ($currentCatId) {
                            return $repo->allExeptCurrent($currentCatId);
                        },
                        'placeholder'   => 'No parent category',
                        'required'      => false,
                        'attr'          => ['class' => 'custom-select']
                    ]
                );
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
            'data_class'   => ProductCategory::class,
            'current_user' => false,
            ]
        );
    }
}

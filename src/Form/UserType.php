<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userName')
            ->add('fullName')
            ->add('email', EmailType::class);
        if ($options['current_user']) {
            if ($options['current_user'] != 'anon.' and in_array('ROLE_ADMIN', $options['current_user']->getRoles())) {
                $builder
                    ->add(
                        'published',
                        CheckboxType::class,
                        [
                            'data'       => true,
                            'label_attr' => ['class' => 'switch-custom']
                        ]
                    );
            } else {
                $builder
                    ->add(
                        'published',
                        HiddenType::class,
                        [
                            'data' => true
                        ]
                    );
            }
        }
        if (isset($options['validation_groups'])) {
            if (in_array('registration', $options['validation_groups'])) {
                $builder
                    ->add(
                        'plainPassword',
                        RepeatedType::class,
                        [
                            'type'           => PasswordType::class,
                            'first_options'  => ['label' => 'Password'],
                            'second_options' => ['label' => 'Confirm password']
                        ]
                    );
            } else {
                // if edition, passwords are not required
                $builder
                    ->add(
                        'plainPassword',
                        RepeatedType::class,
                        [
                            'required'       => false,
                            'type'           => PasswordType::class,
                            'first_options'  => ['label' => 'Password'],
                            'second_options' => ['label' => 'Confirm password']
                        ]
                    );
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'         => User::class,
                'current_user'       => false,
                'translation_domain' => 'forms'
            ]
        );
    }
}

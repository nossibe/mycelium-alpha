<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TransporterRepository")
 */
class Transporter
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $score;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="transporter", cascade={"persist", "remove"})
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="TransportProposal", mappedBy="transporter")
     */
    private $transportProposal;

    /**
     * @ORM\ManyToMany(targetEntity=City::class, cascade={"persist"})
     */
    private $citiesServed;

    /**
     * @ORM\ManyToOne(targetEntity=City::class)
     */
    private $residenceCity;

    /**
     * @ORM\ManyToOne(targetEntity=City::class)
     */
    private $workCity;

    public function __construct()
    {
        $this->transportProposal = new ArrayCollection();
        $this->citiesServed = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(?int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|TransportProposal[]
     */
    public function getTransportProposal(): Collection
    {
        return $this->transportProposal;
    }

    public function addTransportProposal(TransportProposal $transportProposal): self
    {
        if (!$this->transportProposal->contains($transportProposal)) {
            $this->transportProposal[] = $transportProposal;
            $transportProposal->setTransporter($this);
        }

        return $this;
    }

    public function removeTransportProposal(TransportProposal $transportProposal): self
    {
        if ($this->transportProposal->contains($transportProposal)) {
            $this->transportProposal->removeElement($transportProposal);
            // set the owning side to null (unless already changed)
            if ($transportProposal->getTransporter() === $this) {
                $transportProposal->setTransporter(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|City[]
     */
    public function getCitiesServed(): Collection
    {
        return $this->citiesServed;
    }

    public function addCitiesServed(City $citiesServed): self
    {
        if (!$this->citiesServed->contains($citiesServed)) {
            $this->citiesServed[] = $citiesServed;
        }

        return $this;
    }

    public function removeCitiesServed(City $citiesServed): self
    {
        if ($this->citiesServed->contains($citiesServed)) {
            $this->citiesServed->removeElement($citiesServed);
        }

        return $this;
    }

    public function getResidenceCity(): ?City
    {
        return $this->residenceCity;
    }

    public function setResidenceCity(?City $residenceCity): self
    {
        $this->residenceCity = $residenceCity;

        return $this;
    }

    public function getWorkCity(): ?City
    {
        return $this->workCity;
    }

    public function setWorkCity(?City $workCity): self
    {
        $this->workCity = $workCity;

        return $this;
    }
}

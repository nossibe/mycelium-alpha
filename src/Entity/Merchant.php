<?php

namespace App\Entity;

use Cocur\Slugify\Slugify;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MerchantRepository")
 * @Vich\Uploadable()
 */
class Merchant
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="merchant", cascade={"persist"})
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=2000)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $businessName;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SellProposal", mappedBy="seller")
     */
    private $sellProposal;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductSearch", mappedBy="merchant")
     */
    private $productSearches;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ProductCategory", inversedBy="followers")
     */
    private $categoriesFollowed;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slugify;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $paypalEmail;

    /**
     * @ORM\ManyToOne(targetEntity=City::class, inversedBy="merchants")
     * @ORM\JoinColumn(nullable=false)
     */
    private $city;

    /**
     * @var                       string|null
     * @ORM\Column(type="string", length=255)
     */
    private $filename;

    /**
     * @var                                            File|null
     * @Assert\Image(
     *     mimeTypes={"image/*"},
     *     maxSize="5M"
     *     )
     * @Vich\UploadableField(mapping="merchant_image", fileNameProperty="filename")
     */
    private $imageFile;

    /**
     * @var                         DateTimeInterface|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->sellProposal = new ArrayCollection();
        $this->productSearches = new ArrayCollection();
        $this->categoriesFollowed = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getBusinessName(): ?string
    {
        return $this->businessName;
    }

    public function setBusinessName(string $businessName): self
    {
        $this->businessName = $businessName;

        $this->setSlugify($this->businessName);

        return $this;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    public function removeUser(User $user): self
    {

        if ($user->getMerchant() === $this) {
            $user->setMerchant(null);
        }

        return $this;
    }

    /**
     * @return Collection|SellProposal[]
     */
    public function getSellProposal(): Collection
    {
        return $this->sellProposal;
    }

    public function addSellProposal(SellProposal $sellProposal): self
    {
        if (!$this->sellProposal->contains($sellProposal)) {
            $this->sellProposal[] = $sellProposal;
            $sellProposal->setSeller($this);
        }

        return $this;
    }

    public function removeSellProposal(SellProposal $sellProposal): self
    {
        if ($this->sellProposal->contains($sellProposal)) {
            $this->sellProposal->removeElement($sellProposal);
            // set the owning side to null (unless already changed)
            if ($sellProposal->getSeller() === $this) {
                $sellProposal->setSeller(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductSearch[]
     */
    public function getProductSearches(): Collection
    {
        return $this->productSearches;
    }

    public function addProductSearch(ProductSearch $productSearch): self
    {
        if (!$this->productSearches->contains($productSearch)) {
            $this->productSearches[] = $productSearch;
            $productSearch->setMerchant($this);
        }

        return $this;
    }

    public function removeProductSearch(ProductSearch $productSearch): self
    {
        if ($this->productSearches->contains($productSearch)) {
            $this->productSearches->removeElement($productSearch);
            // set the owning side to null (unless already changed)
            if ($productSearch->getMerchant() === $this) {
                $productSearch->setMerchant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductCategory[]
     */
    public function getCategoriesFollowed(): Collection
    {
        return $this->categoriesFollowed;
    }

    public function addCategoriesFollowed(ProductCategory $categoriesFollowed): self
    {
        if (!$this->categoriesFollowed->contains($categoriesFollowed)) {
            $this->categoriesFollowed[] = $categoriesFollowed;
        }

        return $this;
    }

    public function removeCategoriesFollowed(ProductCategory $categoriesFollowed): self
    {
        if ($this->categoriesFollowed->contains($categoriesFollowed)) {
            $this->categoriesFollowed->removeElement($categoriesFollowed);
        }

        return $this;
    }

    public function getSlugify(): ?string
    {
        return $this->slugify;
    }

    public function setSlugify(string $businessName): self
    {
        $slugify = new Slugify();

        $this->slugify = $slugify->slugify($this->businessName);

        return $this;
    }

    public function getPaypalEmail(): ?string
    {
        return $this->paypalEmail;
    }

    public function setPaypalEmail(?string $paypalEmail): self
    {
        $this->paypalEmail = $paypalEmail;

        return $this;
    }

    public function __toString()
    {
        return $this->businessName;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFilename(): ?string
    {
        return $this->filename;
    }

    /**
     * @param  string|null $filename
     * @return Merchant
     */
    public function setFilename(?string $filename): Merchant
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @param  File|null $imageFile
     * @return Merchant
     */
    public function setImageFile(?File $imageFile): Merchant
    {
        $this->imageFile = $imageFile;

        // Only change the updated af if the file is really uploaded to avoid database updates.
        // This is needed when the file should be set when loading the entity.
        if ($this->imageFile instanceof UploadedFile) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @param  DateTimeInterface|null $updatedAt
     * @return Merchant
     */
    public function setUpdatedAt(?DateTimeInterface $updatedAt): Merchant
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}

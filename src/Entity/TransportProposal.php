<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TransportProposalRepository")
 */
class TransportProposal
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $recoveryTime;

    /**
     * @ORM\Column(type="datetime")
     */
    private $deliveryTime;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $buyerAgreement;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $published;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Transporter", inversedBy="transportProposal")
     */
    private $transporter;

    /**
     * @ORM\ManyToOne(targetEntity=TransportSearch::class, inversedBy="transportProposals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $transportSearch;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SellProposal", inversedBy="transportProposal")
     */
    private $sellProposal;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $inDelivering;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $delivered;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRecoveryTime(): ?\DateTimeInterface
    {
        return $this->recoveryTime;
    }

    public function setRecoveryTime($recoveryTime): self
    {
        $this->recoveryTime = new \DateTime($recoveryTime);

        return $this;
    }

    public function getDeliveryTime(): ?\DateTimeInterface
    {
        return $this->deliveryTime;
    }

    public function setDeliveryTime($deliveryTime): self
    {
        $this->deliveryTime = new \DateTime($deliveryTime);

        return $this;
    }

    public function getBuyerAgreement(): ?bool
    {
        return $this->buyerAgreement;
    }

    public function setBuyerAgreement(?bool $buyerAgreement): self
    {
        $this->buyerAgreement = $buyerAgreement;

        return $this;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(?bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    public function getTransporter(): ?Transporter
    {
        return $this->transporter;
    }

    public function setTransporter(?Transporter $transporter): self
    {
        $this->transporter = $transporter;

        return $this;
    }

    public function getTransportSearch(): ?TransportSearch
    {
        return $this->transportSearch;
    }

    public function setTransportSearch(?TransportSearch $transportSearch): self
    {
        $this->transportSearch = $transportSearch;

        return $this;
    }

    public function getSellProposal(): ?SellProposal
    {
        return $this->sellProposal;
    }

    public function setSellProposal(?SellProposal $sellProposal): self
    {
        $this->sellProposal = $sellProposal;

        return $this;
    }

    public function getInDelivering(): ?\DateTimeInterface
    {
        return $this->inDelivering;
    }

    public function setInDelivering(?\DateTimeInterface $inDelivering): self
    {
        $this->inDelivering = $inDelivering;

        return $this;
    }

    public function getDelivered(): ?\DateTimeInterface
    {
        return $this->delivered;
    }

    public function setDelivered(?\DateTimeInterface $delivered): self
    {
        $this->delivered = $delivered;

        return $this;
    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductSearchRepository")
 */
class ProductSearch
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deadLine;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    private $transporterGift;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $published;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Merchant", inversedBy="productSearches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $merchant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     * @Assert\Type(type="App\Entity\Product")
     * @Assert\Valid
     */
    private $product;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SellProposal", mappedBy="productSearch")
     */
    private $sellProposal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\QuantityUnit")
     * @Assert\Type(type="App\Entity\QuantityUnit")
     * @Assert\Valid
     */
    private $quantityUnit;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $clarifications;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $closedDateTime;

    public function __construct()
    {
        $this->sellProposal = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getDeadLine(): ?\DateTimeInterface
    {
        return $this->deadLine;
    }

    public function setDeadLine($deadLine): self
    {
        $this->deadLine = new \DateTime($deadLine);

        return $this;
    }

    public function getTransporterGift(): ?string
    {
        return $this->transporterGift;
    }

    public function setTransporterGift(?string $transporterGift): self
    {
        $this->transporterGift = $transporterGift;

        return $this;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(?bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    /**
     * @return Collection|SellProposal[]
     */
    public function getSellProposal(): Collection
    {
        return $this->sellProposal;
    }

    public function addSellProposal(SellProposal $sellProposal): self
    {
        if (!$this->sellProposal->contains($sellProposal)) {
            $this->sellProposal[] = $sellProposal;
            $sellProposal->setProductSearch($this);
        }

        return $this;
    }

    public function removeSellProposal(SellProposal $sellProposal): self
    {
        if ($this->sellProposal->contains($sellProposal)) {
            $this->sellProposal->removeElement($sellProposal);
            // set the owning side to null (unless already changed)
            if ($sellProposal->getProductSearch() === $this) {
                $sellProposal->setProductSearch(null);
            }
        }

        return $this;
    }

    public function getMerchant(): ?Merchant
    {
        return $this->merchant;
    }

    public function setMerchant(?Merchant $merchant): self
    {
        $this->merchant = $merchant;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(Product $product = null): self
    {
        $this->product = $product;

        return $this;
    }

    public function getQuantityUnit(): ?QuantityUnit
    {
        return $this->quantityUnit;
    }

    public function setQuantityUnit(QuantityUnit $quantityUnit): self
    {
        $this->quantityUnit = $quantityUnit;

        return $this;
    }

    public function getClarifications(): ?string
    {
        return $this->clarifications;
    }

    public function setClarifications(?string $clarifications): self
    {
        $this->clarifications = $clarifications;

        return $this;
    }

    public function getClosedDateTime(): ?\DateTimeInterface
    {
        return $this->closedDateTime;
    }

    public function setClosedDateTime(?\DateTimeInterface $closedDateTime): self
    {
        $this->closedDateTime = $closedDateTime;

        return $this;
    }
}

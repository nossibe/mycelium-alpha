<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SellProposalRepository")
 * @ORM\MappedSuperclass
 */
class SellProposal
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $unitPrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $qtyAvailable;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $published;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Merchant", inversedBy="sellProposal")
     * @ORM\JoinColumn(nullable=false)
     */
    private $seller;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TransportProposal", mappedBy="sellProposal")
     */
    private $transportProposal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProductSearch", inversedBy="sellProposal")
     */
    private $productSearch;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\QuantityUnit")
     * @Assert\Type(type="App\Entity\QuantityUnit")
     * @Assert\Valid
     */
    private $quantityUnit;

    public function __construct()
    {
        $this->transportProposal = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUnitPrice(): ?string
    {
        return $this->unitPrice;
    }

    public function setUnitPrice(string $unitPrice): self
    {
        $this->unitPrice = $unitPrice;

        return $this;
    }

    public function getQtyAvailable(): ?int
    {
        return $this->qtyAvailable;
    }

    public function setQtyAvailable(int $qtyAvailable): self
    {
        $this->qtyAvailable = $qtyAvailable;

        return $this;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(?bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    public function getSeller(): ?Merchant
    {
        return $this->seller;
    }

    public function setSeller(?Merchant $seller): self
    {
        $this->seller = $seller;

        return $this;
    }

    /**
     * @return Collection|TransportProposal[]
     */
    public function getTransportProposal(): Collection
    {
        return $this->transportProposal;
    }

    public function addTransportProposal(TransportProposal $transportProposal): self
    {
        if (!$this->transportProposal->contains($transportProposal)) {
            $this->transportProposal[] = $transportProposal;
            $transportProposal->setSellProposal($this);
        }

        return $this;
    }

    public function removeTransportProposal(TransportProposal $transportProposal): self
    {
        if ($this->transportProposal->contains($transportProposal)) {
            $this->transportProposal->removeElement($transportProposal);
            // set the owning side to null (unless already changed)
            if ($transportProposal->getSellProposal() === $this) {
                $transportProposal->setSellProposal(null);
            }
        }

        return $this;
    }

    public function getProductSearch(): ?ProductSearch
    {
        return $this->productSearch;
    }

    public function setProductSearch(?ProductSearch $productSearch): self
    {
        $this->productSearch = $productSearch;

        return $this;
    }

    public function getQuantityUnit(): ?QuantityUnit
    {
        return $this->quantityUnit;
    }

    public function setQuantityUnit(QuantityUnit $quantityUnit): self
    {
        $this->quantityUnit = $quantityUnit;

        return $this;
    }
}

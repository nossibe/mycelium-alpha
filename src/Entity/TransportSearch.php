<?php

namespace App\Entity;

use App\Repository\TransportSearchRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TransportSearchRepository::class)
 */
class TransportSearch
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published;

    /**
     * @ORM\ManyToOne(targetEntity=SellProposal::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $sellProposal;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\OneToMany(targetEntity=TransportProposal::class, mappedBy="transportSearch")
     */
    private $transportProposals;

    public function __construct()
    {
        $this->transportProposals = new ArrayCollection();
        $this->published = "1";
        $this->date = new \DateTime('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    public function getSellProposal(): ?SellProposal
    {
        return $this->sellProposal;
    }

    public function setSellProposal(?SellProposal $sellProposal): self
    {
        $this->sellProposal = $sellProposal;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection|TransportProposal[]
     */
    public function getTransportProposals(): Collection
    {
        return $this->transportProposals;
    }

    public function addTransportProposal(TransportProposal $transportProposal): self
    {
        if (!$this->transportProposals->contains($transportProposal)) {
            $this->transportProposals[] = $transportProposal;
            $transportProposal->setTransportSearch($this);
        }

        return $this;
    }

    public function removeTransportProposal(TransportProposal $transportProposal): self
    {
        if ($this->transportProposals->contains($transportProposal)) {
            $this->transportProposals->removeElement($transportProposal);
            // set the owning side to null (unless already changed)
            if ($transportProposal->getTransportSearch() === $this) {
                $transportProposal->setTransportSearch(null);
            }
        }

        return $this;
    }
}
